﻿using System.Collections.Generic;
using UnityEngine;

public class Tribe : MonoBehaviour
{

    public string tribeName;
    public List<Character> followers = new List<Character>();
    public List<Character> members = new List<Character>();
    public Character leader;
    public int maxCharacterPerTribe = 50;
    public bool orgyMode;

    public void AddNewRandomMember(Character.Gender gender)
    {
        Character.Attributes attr = CharacterGenerator.instance.GenerateCharacterAttributes(30);
        Character newMember = CharacterGenerator.instance.ChildGender(gender, attr, this);
        newMember.family = new Character.Family();
        newMember = CharacterGenerator.instance.CharacterAssign(newMember, this);
        leader.AddFollower(newMember);
        newMember.ActivateFollowerCharacter();
    }

    public void AddNewRandomMember()
    {
        Character.Attributes attr = CharacterGenerator.instance.GenerateCharacterAttributes(30);
        Character newMember = CharacterGenerator.instance.RandomChildGender(attr, this);
        newMember.family = new Character.Family();
        newMember = CharacterGenerator.instance.CharacterAssign(newMember, this);
        leader.AddFollower(newMember);
        newMember.ActivateFollowerCharacter();
    }

    public void NextGeneration()
    {
        AgingCharacter();
        if (members.Count < maxCharacterPerTribe)
            SexualSelection(FindFathers());
    }
    
    void AgingCharacter()
    {
        for (int i = 0; i < members.Count; i++)
        {
            members[i].age++;
            if (members[i].age == Character.LifeCycle.Dead)
            {
                Debug.Log(members[i].name + " died of old age");
                members[i].Die();
            }
        }
        
    }

    List<Character> FindFathers()
    {
        List<Character> fathers = new List<Character>();
        for (int i = 0; i < members.Count; i++)
        {
            if (members[i].gender == Character.Gender.Male)
                fathers.Add(members[i]);
        }
        return fathers;
    }

    void SexualSelection(List<Character> fathers) // This part will change to assign partners with attributes and a Randomizer
    {
        for (int i = 0; i < fathers.Count; i++)
        {
            for (int j = 0; j < members.Count; j++)
            {
                if (members[j].gender == Character.Gender.Female 
                    && IsNotRelative(members[j], fathers[i]) 
                    && fathers[i].age != Character.LifeCycle.Dead
                    && !fathers[i].hadKids && !members[j].hadKids
                    && members[j].age <= Character.LifeCycle.Adult
                    && members[j].age >= Character.LifeCycle.Teen)
                {
                    fathers[i].hadKids = true;
                    members[j].hadKids = true;
                    CharacterGenerator.instance.CreateNewGeneration(fathers[i], members[j], this);
                }
                    
            }
        }
        ResetGeneration();
    }
    
    void ResetGeneration()
    {
        for (int i = 0; i < members.Count; i++)
            members[i].hadKids = false;
    }

    bool IsNotRelative(Character mother, Character father)
    {
        return (father.family.siblings.Count <= 0 || !father.family.siblings.Contains(mother)) // Not Sister
                && (!father.family.Mother || father.family.Mother != mother) // Not Mother
                && (!father.family.Father || FatherSide(mother, father)) //Not On Father's side
                && (!father.family.Mother || MotherSide(mother, father)) //Not On Mother's side
                && (father.family.children.Count <= 0 || !father.family.children.Contains(mother)) // Not Father's child
                && (mother.family.children.Count <= 0 || !mother.family.children.Contains(father)) // Not Mother's child
                && !IsGrandChild(mother, father); // Not a grandChild
    }

    bool FatherSide(Character mother, Character father)
    {
        return father.family.Father.family.Mother != mother // Not GrandMother
                && !father.family.Father.family.siblings.Contains(mother); // Not Aunt
    }

    bool MotherSide(Character mother, Character father)
    {
        return father.family.Mother.family.Mother != mother // Not GrandMother
                    && !father.family.Mother.family.siblings.Contains(mother); // Not Aunt
    }

    bool IsGrandChild(Character mother, Character father)
    {
        bool found = false;
        for (int i = 0; i < father.family.children.Count; i++)
        {
            for (int j = 0; j < father.family.children[i].family.children.Count; j++)
            {
                if (father.family.children[i].family.children[j] == mother)
                    found = true;
            }
        }
        return found;
    }

    public void AssignNewGenerationToLeader(Character leader, List<Character> newGen)
    {
        for (int k = 0; k < newGen.Count; k++)
        {
            leader.AddFollower(newGen[k]);
            newGen[k].ActivateFollowerCharacter();
        }
    }

    public void RemoveFollower(string name)
    {
        for (int i = 0; i < followers.Count; i++)
        {
            if (followers[i].gameObject.name == name)
            {
                followers.Remove(followers[i]);
                leader.currentDistance -= leader.followDistance;
                return;
            }
        } 
    }

    public void RemoveMember(string name)
    {
        for (int i = 0; i < members.Count; i++)
        {
            if (members[i].gameObject.name == name)
            {
                members.Remove(members[i]);
                return;
            }
        }
    }
    public void AddMembers(Character c)
    {
        members.Add(c);
    }

    public void AssignTribeLeader(Character leader)
    {
        leader.GetComponent<CharacterAIController>().StopAllCoroutines();
        leader.ActivatePlayerCharacter();
        leader.role = Character.Role.Leader;
        this.leader = leader;
        QueueFollowers();
    }

    public void RefreshFollowers()
    {
        if (leader)
        {
            for (int i = 0; i < followers.Count; i++)
            {
                if (followers[i].age == Character.LifeCycle.Dead)
                {
                    for (int j = i + 1; j < followers.Count; j++)
                    {
                        float posX = followers[j].target.localPosition.x + leader.followDistance;
                        followers[j].target.localPosition = new Vector3(posX, followers[j].target.localPosition.y, followers[j].target.localPosition.z);
                    }
                }
            }
        }
    }

    public void QueueFollowers()
    {
        foreach (Character c in followers)
        {
            if (c.name != leader.name && c.age != Character.LifeCycle.Dead)
            {
                leader.AddFollower(c);
                c.ActivateFollowerCharacter();
            }
        }
    }

    public Character NextLeader()
    {
        Character heir = HeirInChildren();
        if (heir)
            return heir;
        else
            return HeirInFollowers();
    }

    Character HeirInFollowers()
    {
        foreach (Character c in followers)
        {
            if (c.age != Character.LifeCycle.Dead)
                return c;
            else if(c) 
                RemoveFollower(c.name);
        }
        return null;
    }

    Character HeirInChildren()
    {
        foreach (Character c in leader.family.children)
        {
            if (c.age != Character.LifeCycle.Dead)
                return c;
            else if(c)
                RemoveFollower(c.name);
        }
        return null;
    }

    public void ToggleEquipFollowers(bool toggle)
    {
        foreach (Character c in followers)
        {
            c.anim.SetBool("Equip", toggle);
            c.equipped = toggle;
        }
    }
}
