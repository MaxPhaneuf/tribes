﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterGenerator : MonoBehaviour
{
    public GameObject characterPrefab, tribePrefab, mainPanel;
    public GameObject[] models;
    public static CharacterGenerator instance;
    public int points = 30;
    public Text fatherText, motherText, fatherName, motherName;
    public InputField numberChildren, tribeName;
    public Button fatherButton, motherButton;
    public Transform tribeSpawn;
    private Character.Attributes fatherAtt, motherAtt;
    private string fName, mName;
    public Transform[] positions;
    public bool hasMother, hasFather;
    public bool autoGenerate;

    public int motherBirthRateEnd = 5, fatherBirthRateCha = 6;

    public static int currentTribeID = 0, currentCharacterID = 0;

    // Use this for initialization
    void Awake()
    {
        if (!instance)
            instance = this;
    }

    private void Start()
    {
        if (autoGenerate)
        {
            CreateTribe();
        }
    }

    public Character.Attributes GenerateCharacterAttributes(Character.Attributes father, Character.Attributes mother)
    {
        Character.Attributes att = new Character.Attributes
        {
            strength = Random.Range(GetLowest(father.strength, mother.strength), GetHighest(father.strength, mother.strength) + 1),
            dexterity = Random.Range(GetLowest(father.dexterity, mother.dexterity), GetHighest(father.dexterity, mother.dexterity) + 1),
            intelligence = Random.Range(GetLowest(father.intelligence, mother.intelligence), GetHighest(father.intelligence, mother.intelligence) + 1),
            perception = Random.Range(GetLowest(father.perception, mother.perception), GetHighest(father.perception, mother.perception) + 1),
            endurance = Random.Range(GetLowest(father.endurance, mother.endurance), GetHighest(father.endurance, mother.endurance) + 1),
            charisma = Random.Range(GetLowest(father.charisma, mother.charisma), GetHighest(father.charisma, mother.charisma) + 1)
        };

        return att;
    }

    public Character.Attributes GenerateCharacterAttributes(int totalPoints)
    {

        int[] tmpList = new int[6];
        int pick;
        if (totalPoints <= 60)
        {
            while (totalPoints > 0)
            {
                pick = Random.Range(0, 6);
                if (tmpList[pick] < 10)
                {
                    tmpList[pick]++;
                    totalPoints--;
                }
            }
        }

        Character.Attributes att = new Character.Attributes
        {
            strength = tmpList[0],
            dexterity = tmpList[1],
            intelligence = tmpList[2],
            perception = tmpList[3],
            endurance = tmpList[4],
            charisma = tmpList[5]
        };

        return att;
    }

    public void CreateFatherButton()
    {
        fatherAtt = GenerateCharacterAttributes(points);
        fatherText.text = fatherAtt.ToString();
        fatherName.text = NameLibrary.MALE_NAMES[Random.Range(0, NameLibrary.MALE_NAMES.Length)];
        fName = fatherName.text;
        hasFather = true;
    }

    public void CreateMotherButton()
    {
        motherAtt = GenerateCharacterAttributes(points);
        motherText.text = motherAtt.ToString();
        motherName.text = NameLibrary.FEMALE_NAMES[Random.Range(0, NameLibrary.FEMALE_NAMES.Length)];
        mName = motherName.text;
        hasMother = true;
    }

    int GetLowest(int father, int mother)
    {
        if (father <= mother)
            return father;
        else
            return mother;

    }

    int GetHighest(int father, int mother)
    {
        if (father >= mother)
            return father;
        else
            return mother;

    }

    void GenerateCurrentFather()
    {
        fatherAtt = GenerateCharacterAttributes(points);
        fName = NameLibrary.MALE_NAMES[Random.Range(0, NameLibrary.MALE_NAMES.Length)];
    }

    void GenerateCurrentMother()
    {
        motherAtt = GenerateCharacterAttributes(points);
        mName = NameLibrary.FEMALE_NAMES[Random.Range(0, NameLibrary.FEMALE_NAMES.Length)];
    }

    public void CreateTribe()
    {
        Tribe currentTribe = TribeInit();
        if (!hasFather)
            GenerateCurrentFather();
        if (!hasMother)
            GenerateCurrentMother();

        Character father = CreateFather(fName, fatherAtt, models[0], currentTribe);
        Character mother = CreateMother(mName, motherAtt, models[1], currentTribe);
        Character.Family parents = new Character.Family { Father = father, Mother = mother };
        Character.Family currentFamily = new Character.Family { children = CreateChildren(PossibleChildren(motherAtt, fatherAtt), parents, currentTribe) };
        father.family = currentFamily;
        mother.family = currentFamily;
        AssignChildrenFamilyMembers(currentFamily, mother, father);
        currentTribe.AssignTribeLeader(father);
        GameManager.instance.tribes.Add(currentTribe.name, currentTribe);
        TribeUI.instance.AssignTribe(currentTribe);
        if (mainPanel.activeInHierarchy)
            mainPanel.SetActive(false);
    }

    Tribe TribeInit()
    {
        Tribe currentTribe = Instantiate(tribePrefab, tribeSpawn.position, tribeSpawn.rotation).GetComponent<Tribe>();
        currentTribeID++;
        if (tribeName.text == "")
            tribeName.text = "Tribe";
        currentTribe.tribeName = tribeName.text;
        currentTribe.name = currentTribe.tribeName + currentTribeID.ToString();
        return currentTribe;
    }

    public int PossibleChildren(Character.Attributes motherAtt, Character.Attributes fatherAtt)
    {
        int numberC = 1;
        if (numberChildren.text == "")
        {
            int motherEndurance = (motherAtt.endurance - motherBirthRateEnd > 0 ? motherAtt.endurance - motherBirthRateEnd : 1);
            int fatherCharisma = (fatherAtt.charisma - fatherBirthRateCha > 0 ? fatherAtt.charisma - fatherBirthRateCha : 1);
            numberC = Random.Range(fatherCharisma, motherEndurance + 1);
        }
        else
            numberC = int.Parse(numberChildren.text);
        return numberC;
    }

    Character CreateFather(string name, Character.Attributes att, GameObject model, Tribe currentTribe)
    {
        Character father = CreateMember(name, 0, model, fatherAtt, Character.Gender.Male, currentTribe.name);
        currentTribe.leader = father;
        AssingCharacterID(father);
        currentTribe.leader = father;
        father.role = Character.Role.Leader;
        father.age = Character.LifeCycle.Teen;
        currentTribe.members.Add(father);
        return father;
    }

    Character CreateMother(string name, Character.Attributes att, GameObject model, Tribe currentTribe)
    {
        Character mother = CreateMember(name, 0, model, att, Character.Gender.Female, currentTribe.name);
        AssingCharacterID(mother);
        mother.maxSpeed = 2;
        currentTribe.followers.Add(mother);
        mother.age = Character.LifeCycle.Teen;
        currentTribe.members.Add(mother);
        return mother;
    }

    public void CreateNewGeneration(Character father, Character mother, Tribe currentTribe)
    {
        Character.Family parents = new Character.Family { Father = father, Mother = mother };
        Character.Family family = new Character.Family
        {
            children = CreateChildren(PossibleChildren(mother.attributes, father.attributes), parents, GameManager.instance.tribes[father.tribeID])
        };
        AssignChildrenFamilyMembers(family, mother, father);
        foreach(Character c in family.children)
        {
            if(!father.family.children.Contains(c))
                father.family.children.Add(c);
            if(!mother.family.children.Contains(c))
                mother.family.children.Add(c);
        }
        Debug.Log(father.age.ToString() + " " + father.name + " and " + mother.age.ToString() + " " + mother.name + " had "
            + family.children.Count.ToString() + (family.children.Count > 1 ? " children" : " child"));
           
        currentTribe.AssignNewGenerationToLeader(currentTribe.leader, family.children);
    }
    
    public List<Character> CreateChildren(int numberC, Character.Family parents, Tribe currentTribe)
    {
        Character.Family currentFamily = new Character.Family();
        for (int i = 0; i < numberC; i++)
        {
            Character.Attributes attr = GenerateCharacterAttributes(parents.Father.attributes, parents.Mother.attributes);
            Character newMember = RandomChildGender(attr, currentTribe);
            if (newMember)
            {
                newMember = CharacterAssign(newMember, currentTribe);
                currentFamily.children.Add(newMember);
            }
        }
        return currentFamily.children;
    }

    public Character CharacterAssign(Character newMember, Tribe currentTribe)
    {
        AssingCharacterID(newMember);
        currentTribe.followers.Add(newMember);
        currentTribe.members.Add(newMember);
        return newMember;
    }

    public void AssignChildrenFamilyMembers(Character.Family currentFamily, Character mother, Character father)
    {
        List<Character> allChildren = new List<Character>();
        foreach(Character c in mother.family.children)
        {
            if (!father.family.children.Contains(c))
                allChildren.Add(c);
        }
        allChildren.AddRange(father.family.children);
        allChildren.AddRange(currentFamily.children);
        for (int i = 0; i < allChildren.Count; i++)
        {
            allChildren[i].family = new Character.Family { Father = father, Mother = mother };
            for (int j = 0; j < allChildren.Count; j++)
            {
                if (allChildren[i] && allChildren[i].name != allChildren[j].name)
                    allChildren[i].family.siblings.Add(allChildren[j]);
            }
        }
    }
    public void DidMotherSurviveLabor(Character mother)
    {
        int chances = Random.Range(1, mother.attributes.endurance);
        if (chances == 1)
            mother.Die();
    }

    public Character ChildGender(Character.Gender gender, Character.Attributes attr, Tribe currentTribe)
    {
        switch (gender)
        {
            case Character.Gender.Male:
                return CreateMember(NameLibrary.MALE_NAMES[Random.Range(0, NameLibrary.MALE_NAMES.Length)], 2, models[0], attr, Character.Gender.Male, currentTribe.name);
            case Character.Gender.Female:
                return CreateMember(NameLibrary.FEMALE_NAMES[Random.Range(0, NameLibrary.FEMALE_NAMES.Length)], 2, models[1], attr, Character.Gender.Female, currentTribe.name);
        }
        return null;
    }

    public Character RandomChildGender(Character.Attributes attr, Tribe currentTribe)
    {
        int pick = Random.Range(0, 2);
        switch (pick)
        {
            case 0:
                return CreateMember(NameLibrary.MALE_NAMES[Random.Range(0, NameLibrary.MALE_NAMES.Length)], 2, models[0], attr, Character.Gender.Male, currentTribe.name);
            case 1:
                return CreateMember(NameLibrary.FEMALE_NAMES[Random.Range(0, NameLibrary.FEMALE_NAMES.Length)], 2, models[1], attr, Character.Gender.Female, currentTribe.name);
        }
        return null;
    }

    Character CreateMember(string name, int position, GameObject model, Character.Attributes attr, Character.Gender gender, string tribeID)
    {
        GameObject newMember = Instantiate(characterPrefab, positions[position].position, positions[position].rotation);
        Character c = newMember.GetComponent<Character>();
        c.attributes = attr;
        c.characterName = name;
        c.healthPoints = (attr.endurance > 0 ? attr.endurance : 1);
        c.healthSlider.maxValue = c.healthPoints;
        c.model = Instantiate(model, newMember.transform);
        c.role = Character.Role.Member;
        c.tribeID = tribeID;
        c.maxSpeed = 2;
        c.gender = gender;
        return c;
    }

    void AssingCharacterID(Character newMember)
    {
        currentCharacterID++;
        newMember.name = newMember.characterName + " " + currentCharacterID;
    }

    public void CancelButton()
    {
        gameObject.SetActive(false);
    }
       
}
