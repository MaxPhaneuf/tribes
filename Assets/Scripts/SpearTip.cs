﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpearTip : MonoBehaviour {

	Character human { get { return transform.parent.GetComponent<Character>(); } }
    bool hit;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.name == "Model" && !hit)
        {
            Character c = null;
            if (collision.transform.parent)
                c = collision.transform.parent.GetComponent<Character>();
            if (c && c.age < Character.LifeCycle.Dead && !c.states.hit && c.currentLevel == human.currentLevel)
            {
                if (c.attributes.type == Character.Type.Human && ((Human)c).currentTribe != ((Human)human).currentTribe
                    || c.attributes.type != Character.Type.Human)
                {
                    c.TakeDamage(human.attributes.damage, human.gameObject);
                    hit = true;
                }
            }
        }
    }

    private void OnDisable()
    {
        hit = false;
    }
}
