﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Meat : MonoBehaviour {

    public Vector2 currentLevel;
    public int meatQuantity;
    public GameObject body;
    public float lastRot, rottingTime = 60;
    public bool fading;
    
    void Start () {
        lastRot = Time.time;
        name = "Meat";
	}
	
	void Update () {
		if(Time.time - lastRot >= rottingTime)
        {
            meatQuantity--;
            lastRot = Time.time;
        }
        if (meatQuantity <= 0)
        {
            World.instance.levels[(int)currentLevel.y][(int)currentLevel.x].meats.Remove(this);
            if (!fading)
            {
                StartCoroutine(FadeOut(true, body.GetComponent<SpriteRenderer>(), .1f));
                Destroy(gameObject, 1);
            }
        }
	}

    public void ToggleView(bool toggle)
    {
        body.SetActive(toggle);
    }

    public IEnumerator FadeOut(bool isFade, SpriteRenderer spr, float speed)
    {
        fading = isFade;
        float a = (isFade ? 0 : 1);
        Color target = new Color(1, 1, 1, a);
        while (Mathf.Abs(spr.color.a - target.a) > 0.1f)
        {
            spr.color = Color.Lerp(spr.color, target, speed);
            yield return new WaitForEndOfFrame();
        }
        spr.color = target;
        fading = !isFade;
    }
}
