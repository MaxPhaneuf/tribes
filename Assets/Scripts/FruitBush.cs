﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FruitBush : MonoBehaviour {

    public GameObject fruitPrefab, bushPrefab, body;
    public Transform fruitsTr;
    public int maxFruits, startingFruits = 3;
    public float lastGrow;
    public float variability = .2f;
    public int growRate;
    public Vector2 fruitSpawnRndPos;
    public Vector2 currentLevel;
    public List<GameObject> fruits = new List<GameObject>();

    void Start()
    {
        growRate = Random.Range(growRate - ((int)(growRate * variability)), growRate + ((int)(growRate * variability)));
        lastGrow = Time.time;
        GrowFruit(Random.Range(1, startingFruits + 1));
        name = "Bush";
    }	
	void Update () {
        if (Time.time - lastGrow >= growRate)
        {
            lastGrow = Time.time;
            GrowFruit(Random.Range(1, maxFruits + 1));
        }
    }

    void GrowFruit(int n)
    {
        if (fruits.Count + n < maxFruits)
        {
            for (int i = 0; i < n; i++)
            {
                GameObject f = RandomizeFruitPosition(Instantiate(fruitPrefab, fruitsTr));
                if (!World.instance.fading)
                {
                    float c = (!World.instance.isDay ? .5f : 1);
                    f.GetComponent<SpriteRenderer>().color = new Color(c, c, c, 1);
                }
                fruits.Add(f);
            }
        }
    }
    public void ToggleView(bool toggle)
    {
        body.SetActive(toggle);
    }

    GameObject RandomizeFruitPosition(GameObject fruit)
    {
        fruit.transform.localPosition = new Vector2(Random.Range(-fruitSpawnRndPos.x, fruitSpawnRndPos.x), Random.Range(-fruitSpawnRndPos.y, fruitSpawnRndPos.y));
        return fruit;
    }
}
