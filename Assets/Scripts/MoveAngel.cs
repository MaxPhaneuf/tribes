﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveAngel : MonoBehaviour {

    Vector3 target;
    bool moving;
    // Use this for initialization
	void Start () {
        target = new Vector3(0, transform.position.y);
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (!moving)
        {
            if (Input.GetKeyDown(KeyCode.LeftArrow) && transform.position.x - 3 >= 0)
                target -= new Vector3(3, 0);
            else if (Input.GetKeyDown(KeyCode.RightArrow) && transform.position.x + 3 <= 39)
                target += new Vector3(3, 0);
        }
        moving = transform.position != target;
                         
        transform.position = Vector3.MoveTowards(transform.position, target, .1f);
    }
}
