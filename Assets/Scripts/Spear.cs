﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spear : MonoBehaviour
{

    public Vector2 currentLevel;
    public int damage = 1;
    public GameObject model;
    public GameObject sender;
    public float grabDistance = .1f;
    bool hit = false;
    Rigidbody2D Rb { get { return GetComponent<Rigidbody2D>(); } }
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        ToggleView(currentLevel == World.instance.currentLevel);
        GameObject obj = LookForSender();
        if (transform.position.x <= World.instance.levels[(int)currentLevel.y][(int)currentLevel.x].bounds.Left || transform.position.x >= World.instance.levels[(int)currentLevel.y][(int)currentLevel.x].bounds.Right || obj)
        {
            Human h = sender.GetComponent<Human>();
            h.thrown = false;
            if (h.Target && Mathf.Abs(h.transform.position.x - h.Target.transform.position.x) >= h.throwDist)
                h.AIThrow = false;
            Destroy(gameObject);
        }
    }

    GameObject LookForSender()
    {
        GameObject tmp = null;
        float dist = float.MaxValue;
        foreach (Character c in World.instance.levels[(int)currentLevel.y][(int)currentLevel.x].characters)
        {
            if (c)
                dist = Mathf.Abs(transform.position.x - c.transform.position.x);
            if (sender && c && c.name == sender.name && dist <= grabDistance && !sender.GetComponent<Human>().cooldown)
                tmp = c.gameObject;
        }
        return tmp;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (!Rb.isKinematic)
        {
            Rb.velocity = Vector2.zero;
            Rb.isKinematic = true;
            transform.Find("Collider").GetComponent<CircleCollider2D>().isTrigger = true;
        }
        hit = true;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.name == "Model" && !hit)
        {
            Character c = null;
            if (collision.transform.parent)
                c = collision.transform.parent.GetComponent<Character>();
            if (c && c.age < Character.LifeCycle.Dead
                && !c.states.hit && c.currentHP > 0
                && c.currentLevel == currentLevel
                && c.name != sender.name)
            {
                if (c.attributes.type == Character.Type.Human && c.GetComponent<Human>().currentTribe != sender.GetComponent<Human>().currentTribe
                    || c.attributes.type != Character.Type.Human)
                {
                    c.TakeDamage(damage, sender);
                    Rb.velocity = Vector2.zero;
                    hit = true;
                }

            }
        }
    }

    public void ToggleView(bool toggle)
    {
        model.SetActive(toggle);
    }

}
