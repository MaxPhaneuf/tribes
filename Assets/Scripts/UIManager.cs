﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    const float COVER_SPEED = .1f;
    public static UIManager instance;
    public GameObject MainMenu, OptionMenu, Direction, Mark, Debug, Sound, characterCreation;
    public Text currentTime, currentPosition;
    public bool active, goingLeft, goingRight, goingUp, goingDown, playingAmbience = true, playingBGM;
    public Button Up, Down, Left, Right;
    public Image hunting;
    public Sprites sprites;
    public Toggle debugToggle, music;
    public Counters counters;
    public AudioSource musicSource, ambientSource;
    public Vector2 lastLevel;
    public Image coverImage;
    public bool covered;
    public CharacterCreation cc;
    [System.Serializable]
    public class CharacterCreation
    {
        [System.Serializable]
        public struct Male
        {
            public int BodyIndex, HairStyleIndex, HairToneIndex;
        }

        [System.Serializable]
        public struct Female
        {
            public int BodyIndex, HairStyleIndex, HairToneIndex;
        }
        public Male male;
        public Female female;
    }

    [System.Serializable]
    public class Counters
    {
        [System.Serializable]
        public class World
        {
            public Text frogCount, rabbitCount, snakeCount, goatCount, dearCount, wolfCount, bearCount;
        }

        [System.Serializable]
        public class Level
        {
            public Text frogCount, rabbitCount, snakeCount, goatCount, dearCount, wolfCount, bearCount;
        }
        public World worldCounters;
        public Level levelCounters;
    }

    [System.Serializable]
    public class Sprites
    {
        public Animals animals;
        [System.Serializable]
        public class Animals
        {
            public Sprite Frog, Rabbit, Snake, Goat, Dear, Wolf, Bear;
        }
    }
    void Start()
    {
        if (!instance)
        {
            DontDestroyOnLoad(gameObject);
            instance = this;
        }
        else
            Destroy(gameObject);
        playingAmbience = true;
        playingBGM = true;
        cc.male.HairStyleIndex = 3;
        cc.female.HairStyleIndex = 1;
    }
    
    private void Update()
    {
        if (!World.instance.started && Input.GetKeyDown(KeyCode.Escape))
        {
            if (Sound.activeInHierarchy)
                Sound.SetActive(false);
            else if (OptionMenu.activeInHierarchy)
                OptionMenu.SetActive(false);
        }

        if (World.instance.started)
        {
            if (!active)
            {
                currentTime.gameObject.SetActive(true);
                currentPosition.gameObject.SetActive(true);
                active = true;
            }
            currentTime.text = World.instance.minutes + " Mins " + World.instance.seconds + " Secs ";
            currentPosition.text = World.instance.currentLevel.x.ToString() + " : " + World.instance.currentLevel.y.ToString();
            UpdateCounter();
            if (active && !World.instance.busy)
            {
                if (Input.GetKeyDown(KeyCode.Escape))
                {
                    if (Sound.activeInHierarchy)
                        Sound.SetActive(false);
                    else
                    {
                        OptionMenu.SetActive(!OptionMenu.activeInHierarchy);
                    }

                }
            }
            if (World.instance.debug)
                Debug.SetActive(!OptionMenu.activeInHierarchy);
        }
    }

    public IEnumerator ToggleCover(bool isCover)
    {
        float a = (isCover ? 1 : 0);
        Color target = new Color(0, 0, 0, a);
        while (Mathf.Abs(coverImage.color.a - target.a) >= COVER_SPEED)
        {
            coverImage.color = Color.Lerp(coverImage.color, target, COVER_SPEED);
            yield return new WaitForEndOfFrame();
        }
        coverImage.color = target;
        covered = isCover;
    }

    #region Button
    public void StartGame()
    {
        MainMenu.SetActive(false);
        characterCreation.SetActive(false);
        StartCoroutine(World.instance.StartGame());
    }

    public void CloseMenu(GameObject menu)
    {
        menu.SetActive(false);
    }

    public void OpenMenu(GameObject menu)
    {
        menu.SetActive(true);
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void Restart()
    {
        if (World.instance.started && SceneManager.GetActiveScene().isLoaded)
        {
            StartCoroutine(ToggleCover(true));
            if (World.instance.debug)
                DebugMode();
            Destroy(World.instance);
            SceneManager.LoadScene("Transition");
            Sound.SetActive(false);
            OptionMenu.SetActive(false);
            MainMenu.SetActive(true);

            Debug.SetActive(false);
        }
    }
    #endregion

    #region Migration
    void MigrateButton(int x, int y)
    {
        Vector2 lvl = new Vector2(x, y);
        if(Direction.activeInHierarchy)
            Direction.SetActive(false);
        StartCoroutine(World.instance.MoveToLevel(lvl));
    }

    public void RightButton()
    {
        Vector2 currentLevel = Camera2DFollow.instance.target.GetComponent<Character>().currentLevel;
        if (!goingRight)
        {
            goingLeft = false;
            goingRight = true;
            goingDown = false;
            goingUp = false;
            ShowSpriteByType(new Vector2(currentLevel.x + 1, currentLevel.y));
        }
        else
        {
            goingRight = false;
            World.instance.horizontalMove = true;
            World.instance.Player.GetComponent<Animal>().migrationDirection = Character.Direction.East;
            MigrateButton((int)currentLevel.x + 1, (int)currentLevel.y);
        }
    }

    public void DownButton()
    {
        Vector2 currentLevel = Camera2DFollow.instance.target.GetComponent<Character>().currentLevel;
        if (!goingDown)
        {
            goingLeft = false;
            goingRight = false;
            goingDown = true;
            goingUp = false;
            ShowSpriteByType(new Vector2(currentLevel.x, currentLevel.y + 1));
        }
        else
        {
            goingDown = false;
            World.instance.horizontalMove = false;
            World.instance.Player.GetComponent<Animal>().migrationDirection = Character.Direction.South;
            MigrateButton((int)currentLevel.x, (int)currentLevel.y + 1);
        }
    }

    public void LeftButton()
    {
        Vector2 currentLevel = Camera2DFollow.instance.target.GetComponent<Character>().currentLevel;
        if (!goingLeft)
        {
            goingLeft = true;
            goingRight = false;
            goingDown = false;
            goingUp = false;
            ShowSpriteByType(new Vector2(currentLevel.x - 1, currentLevel.y));
        }
        else
        {
            goingLeft = false;
            World.instance.horizontalMove = true;
            World.instance.Player.GetComponent<Animal>().migrationDirection = Character.Direction.West;
            MigrateButton((int)currentLevel.x - 1, (int)currentLevel.y);
        }

    }

    public void UpButton()
    {
        Vector2 currentLevel = Camera2DFollow.instance.target.GetComponent<Character>().currentLevel;
        if (!goingUp)
        {
            goingUp = true;
            goingDown = false;
            goingLeft = false;
            goingRight = false;
            ShowSpriteByType(new Vector2(currentLevel.x, currentLevel.y - 1));
        }
        else
        {
            goingUp = false;
            World.instance.horizontalMove = false;
            World.instance.Player.GetComponent<Animal>().migrationDirection = Character.Direction.North;
            MigrateButton((int)currentLevel.x, (int)currentLevel.y - 1);
        }

    }

    public void UpdateMigrationButton()
    {
        Vector2 currentLevel = World.instance.currentLevel;
        World.Options.WorldInstance world = World.instance.options.currentWorldInstance;
        if (currentLevel.x + 1 >= world.sizeX)
            Right.interactable = false;
        if (currentLevel.y + 1 >= world.sizeY)
            Down.interactable = false;
        if (currentLevel.x - 1 < 0)
            Left.interactable = false;
        if (currentLevel.y - 1 < 0)
            Up.interactable = false;

    }

    public void CancelMigration()
    {
        Camera2DFollow.instance.target.gameObject.SetActive(true);
        Direction.SetActive(false);
        float x = Camera2DFollow.instance.target.position.x;
        if (x < 0)
            StartCoroutine(Camera2DFollow.instance.target.GetComponent<Human>().MoveAtPosition(x + 5, 1));
        else if (x > 0)
            StartCoroutine(Camera2DFollow.instance.target.GetComponent<Human>().MoveAtPosition(x - 5, 1));
    }
    #endregion

    #region Debug
    void ShowSpriteByType(Vector2 lvl)
    {
        if (World.instance.levels[(int)lvl.y][(int)lvl.x].characters.Count > 0)
        {
            Character.Type type = World.instance.MostCharacterInLevel(lvl);
            if (type == Character.Type.Frog)
                hunting.sprite = sprites.animals.Frog;
            else if (type == Character.Type.Rabbit)
                hunting.sprite = sprites.animals.Rabbit;
            else if (type == Character.Type.Snake)
                hunting.sprite = sprites.animals.Snake;
            else if (type == Character.Type.Goat)
                hunting.sprite = sprites.animals.Goat;
            else if (type == Character.Type.Dear)
                hunting.sprite = sprites.animals.Dear;
            else if (type == Character.Type.Wolf)
                hunting.sprite = sprites.animals.Wolf;
            else if (type == Character.Type.Bear)
                hunting.sprite = sprites.animals.Bear;
            Mark.SetActive(false);
            hunting.enabled = true;
        }
        else
        {
            Mark.SetActive(true);
            hunting.enabled = false;
        }
    }
    
    public void DebugMode()
    {
        if (World.instance.started)
        {
            Debug.SetActive(!World.instance.debug);
            World.instance.debug = !World.instance.debug;
            if (!World.instance.debug && World.instance.Player)
            {
                World.instance.Player.GetComponent<Character>().state = Character.State.Controlled;
                Camera2DFollow.instance.target = World.instance.Player.transform;
                StartCoroutine(World.instance.MoveToLevel(World.instance.Player.GetComponent<Character>().currentLevel));
            }
            else if (World.instance.Player)
                World.instance.Player.GetComponent<Character>().state = Character.State.Moving;
        }
        else
            debugToggle.isOn = false;
    }

    public void UpdateCounter()
    {
        counters.worldCounters.frogCount.text = World.instance.currentCharacters.Frog.ToString();
        counters.worldCounters.rabbitCount.text = World.instance.currentCharacters.Rabbit.ToString();
        counters.worldCounters.snakeCount.text = World.instance.currentCharacters.Snake.ToString();
        counters.worldCounters.goatCount.text = World.instance.currentCharacters.Goat.ToString();
        counters.worldCounters.dearCount.text = World.instance.currentCharacters.Dear.ToString();
        counters.worldCounters.wolfCount.text = World.instance.currentCharacters.Wolf.ToString();
        counters.worldCounters.bearCount.text = World.instance.currentCharacters.Bear.ToString();

        Vector2 lvl = World.instance.currentLevel;
        int[] list = World.instance.CountCharactersInLevel(lvl);
        if (list[0] > 0)
            counters.levelCounters.frogCount.text = list[0].ToString();
        else
            counters.levelCounters.frogCount.text = "0";

        if (list[1] > 0)
            counters.levelCounters.rabbitCount.text = list[1].ToString();
        else
            counters.levelCounters.rabbitCount.text = "0";

        if (list[2] > 0)
            counters.levelCounters.snakeCount.text = list[2].ToString();
        else
            counters.levelCounters.snakeCount.text = "0";

        if (list[3] > 0)
            counters.levelCounters.goatCount.text = list[3].ToString();
        else
            counters.levelCounters.goatCount.text = "0";

        if (list[4] > 0)
            counters.levelCounters.dearCount.text = list[4].ToString();
        else
            counters.levelCounters.dearCount.text = "0";

        if (list[5] > 0)
            counters.levelCounters.wolfCount.text = list[5].ToString();
        else
            counters.levelCounters.wolfCount.text = "0";

        if (list[6] > 0)
            counters.levelCounters.bearCount.text = list[6].ToString();
        else
            counters.levelCounters.bearCount.text = "0";
    }
    #endregion

    #region Sound
    public void MusicToggle()
    {
        if (musicSource.isPlaying)
        {
            playingBGM = false;
            musicSource.Stop();
        }
        else
        {
            playingBGM = true;
            musicSource.Play();
        }
    }

    public void AmbientToggle()
    {
        playingAmbience = !playingAmbience;
        if (!playingAmbience && ambientSource.isPlaying)
            ambientSource.Stop();
        else if (playingAmbience && !ambientSource.isPlaying)
            ambientSource.Play();
    }
    #endregion

    #region Character Creation
    public void NextMaleBody(Transform tr)
    {
        tr.GetChild(cc.male.BodyIndex).gameObject.SetActive(false);
        cc.male.BodyIndex++;
        if (cc.male.BodyIndex >= tr.childCount)
            cc.male.BodyIndex = 0;
        tr.GetChild(cc.male.BodyIndex).gameObject.SetActive(true);
    }

    public void NextMaleHair(Transform tr)
    {
        tr.GetChild(cc.male.HairStyleIndex).GetChild(cc.male.HairToneIndex).gameObject.SetActive(false);
        cc.male.HairToneIndex++;
        if (cc.male.HairToneIndex >= tr.GetChild(cc.male.HairStyleIndex).childCount)
        {
            cc.male.HairStyleIndex++;
            cc.male.HairToneIndex = 0;
            if (cc.male.HairStyleIndex >= tr.childCount)
                cc.male.HairStyleIndex = 0;
        }
        tr.GetChild(cc.male.HairStyleIndex).GetChild(cc.male.HairToneIndex).gameObject.SetActive(true);
    }

    public void PreviousMaleBody(Transform tr)
    {
        tr.GetChild(cc.male.BodyIndex).gameObject.SetActive(false);
        cc.male.BodyIndex--;
        if (cc.male.BodyIndex < 0)
            cc.male.BodyIndex = tr.childCount - 1;
        tr.GetChild(cc.male.BodyIndex).gameObject.SetActive(true);
    }

    public void PreviousMaleHair(Transform tr)
    {
        tr.GetChild(cc.male.HairStyleIndex).GetChild(cc.male.HairToneIndex).gameObject.SetActive(false);
        cc.male.HairToneIndex--;
        if (cc.male.HairToneIndex < 0 )
        {
            cc.male.HairStyleIndex--;
            if (cc.male.HairStyleIndex < 0)
                cc.male.HairStyleIndex = tr.childCount - 1;
            cc.male.HairToneIndex = tr.GetChild(cc.male.HairStyleIndex).childCount - 1;
            
        }
        tr.GetChild(cc.male.HairStyleIndex).GetChild(cc.male.HairToneIndex).gameObject.SetActive(true);
    }

    public void NextFemaleBody(Transform tr)
    {
        tr.GetChild(cc.female.BodyIndex).gameObject.SetActive(false);
        cc.female.BodyIndex++;
        if (cc.female.BodyIndex >= tr.childCount)
            cc.female.BodyIndex = 0;
        tr.GetChild(cc.female.BodyIndex).gameObject.SetActive(true);
    }

    public void NextFemaleHair(Transform tr)
    {
        tr.GetChild(cc.female.HairStyleIndex).GetChild(cc.female.HairToneIndex).gameObject.SetActive(false);
        cc.female.HairToneIndex++;
        if (cc.female.HairToneIndex >= tr.GetChild(cc.female.HairStyleIndex).childCount)
        {
            cc.female.HairStyleIndex++;
            cc.female.HairToneIndex = 0;
            if (cc.female.HairStyleIndex >= tr.childCount)
                cc.female.HairStyleIndex = 0;
        }
        tr.GetChild(cc.female.HairStyleIndex).GetChild(cc.female.HairToneIndex).gameObject.SetActive(true);
    }

    public void PreviousFemaleBody(Transform tr)
    {
        tr.GetChild(cc.female.BodyIndex).gameObject.SetActive(false);
        cc.female.BodyIndex--;
        if (cc.female.BodyIndex < 0)
            cc.female.BodyIndex = tr.childCount - 1;
        tr.GetChild(cc.female.BodyIndex).gameObject.SetActive(true);
    }

    public void PreviousFemaleHair(Transform tr)
    {
        tr.GetChild(cc.female.HairStyleIndex).GetChild(cc.female.HairToneIndex).gameObject.SetActive(false);
        cc.female.HairToneIndex--;
        if (cc.female.HairToneIndex < 0)
        {
            cc.female.HairStyleIndex--;
            if (cc.female.HairStyleIndex < 0)
                cc.female.HairStyleIndex = tr.childCount - 1;
            cc.female.HairToneIndex = tr.GetChild(cc.female.HairStyleIndex).childCount - 1;

        }
        tr.GetChild(cc.female.HairStyleIndex).GetChild(cc.female.HairToneIndex).gameObject.SetActive(true);
    }
    #endregion

}
