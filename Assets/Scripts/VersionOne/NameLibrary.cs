﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NameLibrary {

    public static string[] MALE_NAMES = 
    {
        "George", "Mike", "Robert", "Ronak", "Bismark", "Adolf", "Michel", "Numbnar", "Loknar", "Pierre", "Anatole", "Frisk", "John", "Ugor", "Markor"
    };
    public static string[] FEMALE_NAMES = 
    {
        "Berta", "Sophia", "Skylar", "Yosuno", "Ida", "Marla", "Claire", "Zia", "Lula", "Shalia", "Sarah", "Ana", "Lola", "Guerda", "Zoa", "Alexandra"
    };

}
