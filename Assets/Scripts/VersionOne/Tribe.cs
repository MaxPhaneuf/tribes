﻿using System.Collections.Generic;
using UnityEngine;

public class Tribe : MonoBehaviour
{

    public string tribeName;
    public List<CharacterOne> followers = new List<CharacterOne>();
    public List<CharacterOne> members = new List<CharacterOne>();
    public List<string> alliedTribes = new List<string>();
    public CharacterOne leader;
    public bool orgyMode;
    public static int currentTribeID = 0;

    public static Tribe InitTribe(GameObject tribePrefab, string tribeName)
    {
        Tribe currentTribe = Instantiate(tribePrefab, GameManager.instance.transform).GetComponent<Tribe>();
        currentTribeID++;
        currentTribe.tribeName = tribeName;
        currentTribe.name = currentTribe.tribeName + currentTribeID.ToString();
        return currentTribe;
    }

    public void AddNewRandomMember(CharacterOne.Gender gender)
    {
        CharacterOne.Attributes attr = CharacterGenerator.instance.GenerateCharacterAttributes(30);
        CharacterOne newMember = CharacterGenerator.instance.CreateByGender(gender, attr, this);
        newMember.family = new CharacterOne.Family();
        newMember = CharacterGenerator.instance.CharacterAssign(newMember, this);
        leader.AddFollower(newMember);
        newMember.ActivateFollowerCharacter();
    }

    public void AddNewRandomMember()
    {
        CharacterOne.Attributes attr = CharacterGenerator.instance.GenerateCharacterAttributes(30);
        CharacterOne newMember = CharacterGenerator.instance.CreateWithRandomGender(attr, this);
        newMember.family = new CharacterOne.Family();
        newMember = CharacterGenerator.instance.CharacterAssign(newMember, this);
        leader.AddFollower(newMember);
        newMember.ActivateFollowerCharacter();
    }

    public void NextGeneration()
    {
        AgingCharacter();
        if (members.Count < GameManager.instance.maxCharacterPerTribe)
            SexualSelection(FindFathers());
    }
    
    void AgingCharacter()
    {
        List<CharacterOne> dying = new List<CharacterOne>();
        for (int i = 0; i < members.Count; i++)
        {
            members[i].currentYears++;
            members[i].attributes.healthPoints = (members[i].attributes.endurance > 0 ? members[i].attributes.endurance : 1);
            if (members[i].currentYears >= GameManager.instance.yearsPerGeneration)
            {
                members[i].currentYears = 0;
                members[i].age++;
                Debug.Log(members[i].name + " became " + members[i].age.ToString());
            }
            ChangeCharacterModel(members[i]);
            if (members[i].age == CharacterOne.LifeCycle.Dead)
            {
                members[i].Die(true);
                dying.Add(members[i]);
            }
        }
        for (int i = 0; i < dying.Count; i++)
        {
            dying[i].RemoveAndDestroy();
        }
        
    }

    void ChangeCharacterModel(CharacterOne c)
    {
        if (c.age == CharacterOne.LifeCycle.Teen)
            c.model.transform.localScale = new Vector3(1, 1, 1);
        if (c.gender == CharacterOne.Gender.Male)
        {
            if (c.age == CharacterOne.LifeCycle.Adult)
                CreateNewModel(c, CharacterGenerator.instance.maleModels[1]);
            if(c.age == CharacterOne.LifeCycle.Old)
                CreateNewModel(c, CharacterGenerator.instance.maleModels[2]);
            if (c.age == CharacterOne.LifeCycle.Ancient)
                c.model.transform.localScale = new Vector3(.9f, 1, 1);
        }
        
    }
    void CreateNewModel(CharacterOne c, GameObject model)
    {
        Destroy(c.model);
        c.model = Instantiate(model, c.transform);
        c.anim = c.model.GetComponent<Animator>();
    }

    List<CharacterOne> FindFathers()
    {
        List<CharacterOne> fathers = new List<CharacterOne>();
        for (int i = 0; i < members.Count; i++)
        {
            if (members[i].gender == CharacterOne.Gender.Male)
                fathers.Add(members[i]);
        }
        return fathers;
    }

    void SexualSelection(List<CharacterOne> fathers) // This part will change to assign partners with attributes and a Randomizer
    {
        for (int i = 0; i < fathers.Count; i++)
        {
            for (int j = 0; j < members.Count; j++)
            {
                if (members[j].gender == CharacterOne.Gender.Female 
                    && IsNotRelative(members[j], fathers[i]) 
                    && fathers[i].age != CharacterOne.LifeCycle.Dead
                    && !fathers[i].hadKids && !members[j].hadKids
                    && members[j].age <= CharacterOne.LifeCycle.Adult
                    && members[j].age >= CharacterOne.LifeCycle.Teen)
                {
                    fathers[i].hadKids = true;
                    members[j].hadKids = true;
                    CharacterGenerator.instance.CreateNewGeneration(fathers[i], members[j], this);
                }
                    
            }
        }
        ResetGeneration();

    }
    
    void ResetGeneration()
    {
        for (int i = 0; i < members.Count; i++)
            members[i].hadKids = false;
    }

    bool IsNotRelative(CharacterOne mother, CharacterOne father)
    {
        return (father.family.siblings.Count <= 0 || !father.family.siblings.Contains(mother)) // Not Sister
                && (!father.family.Mother || father.family.Mother != mother) // Not Mother
                && (!father.family.Father || FatherSide(mother, father)) //Not On Father's side
                && (!father.family.Mother || MotherSide(mother, father)) //Not On Mother's side
                && (father.family.children.Count <= 0 || !father.family.children.Contains(mother)) // Not Father's child
                && (mother.family.children.Count <= 0 || !mother.family.children.Contains(father)) // Not Mother's child
                && !IsGrandChild(mother, father); // Not a grandChild
    }

    bool FatherSide(CharacterOne mother, CharacterOne father)
    {
        return father.family.Father.family.Mother != mother // Not GrandMother
                && !father.family.Father.family.siblings.Contains(mother); // Not Aunt
    }

    bool MotherSide(CharacterOne mother, CharacterOne father)
    {
        return father.family.Mother.family.Mother != mother // Not GrandMother
                    && !father.family.Mother.family.siblings.Contains(mother); // Not Aunt
    }

    bool IsGrandChild(CharacterOne mother, CharacterOne father)
    {
        bool found = false;
        for (int i = 0; i < father.family.children.Count; i++)
        {
            for (int j = 0; j < father.family.children[i].family.children.Count; j++)
            {
                if (father.family.children[i].family.children[j] == mother)
                    found = true;
            }
        }
        return found;
    }

    public void AssignNewGenerationToLeader(CharacterOne leader, List<CharacterOne> newGen)
    {
        for (int k = 0; k < newGen.Count; k++)
        {
            leader.AddFollower(newGen[k]);
            newGen[k].ActivateFollowerCharacter();
        }
    }

    public void RemoveFollower(string name)
    {
        for (int i = 0; i < followers.Count; i++)
        {
            if (followers[i].gameObject.name == name)
            {
                followers.Remove(followers[i]);
                leader.currentDistance -= leader.followDistance;
                return;
            }
        } 
    }

    public void RemoveMember(string name)
    {
        for (int i = 0; i < members.Count; i++)
        {
            if (members[i].gameObject.name == name)
            {
                members.Remove(members[i]);
                return;
            }
        }
    }
    public void AddMembers(CharacterOne c)
    {
        members.Add(c);
    }

    public void AssignTribeLeader(CharacterOne leader, bool isPlayer)
    {
        leader.GetComponent<CharacterAIController>().StopAllCoroutines();
        if(isPlayer)
            leader.ActivatePlayerCharacter();
        leader.role = CharacterOne.Role.Leader;
        this.leader = leader;
        QueueFollowers();
    }

    public void RefreshFollowers()
    {
        if (leader)
        {
            for (int i = 0; i < followers.Count; i++)
            {
                if (followers[i].age == CharacterOne.LifeCycle.Dead)
                {
                    for (int j = i + 1; j < followers.Count; j++)
                    {
                        float posX = followers[j].target.localPosition.x + leader.followDistance;
                        followers[j].target.localPosition = new Vector3(posX, followers[j].target.localPosition.y, followers[j].target.localPosition.z);
                    }
                }
            }
        }
    }

    public void QueueFollowers()
    {
        foreach (CharacterOne c in followers)
        {
            if (c.name != leader.name && c.age != CharacterOne.LifeCycle.Dead)
            {
                leader.AddFollower(c);
                c.ActivateFollowerCharacter();
            }
        }
    }

    public CharacterOne NextLeader()
    {
        CharacterOne heir = HeirInGender(CharacterOne.Gender.Male);
        if (heir)
            return heir;
        else
        {
            heir = HeirInGender(CharacterOne.Gender.Female);
            if (heir)
                return heir;
            else
                return HeirInFollowers();
        }
    }

    CharacterOne HeirInFollowers()
    {
        foreach (CharacterOne c in followers)
        {
            if (c.age != CharacterOne.LifeCycle.Dead)
                return c;
        }
        return null;
    }

    CharacterOne HeirInChildren()
    {
        foreach (CharacterOne c in leader.family.children)
        {
            if (c.age != CharacterOne.LifeCycle.Dead)
                return c;
        }
        return null;
    }

    CharacterOne HeirInGender(CharacterOne.Gender gender)
    {
        foreach (CharacterOne c in leader.family.children)
        {
            if (c.age != CharacterOne.LifeCycle.Dead && c.gender == gender)
                return c;
        }
        return null;
    }
    
    public void ToggleEquipFollowers(bool toggle)
    {
        foreach (CharacterOne c in followers)
        {
            if (c.age > CharacterOne.LifeCycle.Child)
            {
                c.anim.SetBool("Equip", toggle);
                c.equipped = toggle;
            }
        }
    }
}
