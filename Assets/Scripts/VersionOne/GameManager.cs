﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public Dictionary<string, Tribe> tribes = new Dictionary<string, Tribe>();
    public static GameManager instance;
    public int currentYear = 0;
    public int yearsPerGeneration = 3;
    public int maxCharacterPerTribe = 50;
    
    void Awake()
    {
        if (!instance)
            instance = this;
        else
            Destroy(gameObject);
        DontDestroyOnLoad(gameObject);
    }

    public void PassTime(string tribeID)
    {
        currentYear++;
        tribes[tribeID].NextGeneration();
    }

    public void NewMan(string tribeID)
    {
        tribes[tribeID].AddNewRandomMember(CharacterOne.Gender.Male);
    }

    public void NewWomen(string tribeID)
    {
        tribes[tribeID].AddNewRandomMember(CharacterOne.Gender.Female);
    }

    public void ChangeScene(Transform spawnPoint, Collider2D collision, string tribeID)
    {
        CoverScreen(tribeID, collision, spawnPoint);
        CharacterGenerator.instance.autoGenerate = false;
    }

    void CoverScreen(string tribeID, Collider2D collision, Transform spawnPoint)
    {
        TribeUI.instance.coverUI.gameObject.SetActive(true);
        tribes[collision.GetComponentInParent<CharacterOne>().tribeID].leader.transform.position = new Vector2(spawnPoint.position.x, tribes[collision.GetComponentInParent<CharacterOne>().tribeID].leader.transform.position.y);
        List<CharacterOne> list = tribes[tribeID].followers;
        for (int i = 0; i < list.Count; i++)
        {
            list[i].transform.position = new Vector2(list[i].target.position.x, list[i].transform.position.y);
        }
        StartCoroutine(Timer(.75f, tribeID));
        list = tribes[tribeID].members;
        for (int i = 0; i < list.Count; i++)
        {
            DontDestroyOnLoad(list[i]);
        }
        SceneManager.LoadScene(0);
    }

    IEnumerator Timer(float waitTime, string tribeID)
    {
        float counter = 0;
        while (counter < waitTime)
        {
            counter += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        PassTime(tribeID);
        TribeUI.instance.coverUI.gameObject.SetActive(false);
    }

}
