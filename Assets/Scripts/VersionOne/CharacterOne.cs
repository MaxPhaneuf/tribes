using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class CharacterOne : MonoBehaviour
{
    public static string MELEE = "Melee";
    public static string RANGE = "Range";
    public static string EQUIP = "Equip";
    public static string PRAY = "Pray";

    public class Attributes
    {
        public int strength, dexterity, intelligence, perception, endurance, charisma;
        public int healthPoints;
        public string characterName = "Default";

        public override string ToString()
        {
            return strength.ToString() + "\n"
            + dexterity.ToString() + "\n"
            + intelligence.ToString() + "\n"
            + perception.ToString() + "\n"
            + endurance.ToString() + "\n"
            + charisma.ToString();
        }
    }

    [System.Serializable]
    public class Family
    {
        public CharacterOne Father;
        public CharacterOne Mother;
        public List<CharacterOne> siblings = new List<CharacterOne>();
        public List<CharacterOne> children = new List<CharacterOne>();

        public void RemoveFamilyLink(CharacterOne from)
        {
            if (Father)
                Father.family.children.Remove(from);

            if (Mother)
                Mother.family.children.Remove(from);
            for (int i = 0; i < siblings.Count; i++)
            {
                if (siblings.Count > 0 && siblings[i])
                    siblings[i].family.siblings.Remove(from);
            }
            for (int j = 0; j < children.Count; j++)
            {
                if (children[j])
                {
                    if (from.gender == Gender.Male)
                        children[j].family.Father = null;
                    else
                        children[j].family.Mother = null;
                }
            }
        }
    }

    public enum Role { Leader, Member }
    public enum Gender { Male, Female }
    public enum LifeCycle { Child, Teen, Adult, MidLife, Old, Ancient, Dead }

    public string tribeID;
    public Attributes attributes;
    public Family family;
    public GameObject model;
    public int modelIndex = 0;
    public List<string> traits = new List<string>();
    public bool attacking, straff, grounded, hit, facingRight = true, airControl = false;
    public Transform attackPoint, groundCheck, target;
    public float attackRadius, hitTimer = .1f, maxSpeed = 10f, jumpForce = 400f, groundedRadius = .2f;
    public int attackMin;
    public LayerMask targetMask, whatIsGround;
    public float enemyFollowDistance;
    public bool jump, melee, equip, range, order, acted, equipped, charge, hadKids;
    public Role role = Role.Member;
    public Gender gender = Gender.Male;
    public LifeCycle age = LifeCycle.Child;
    public int currentYears = 0;
    public Slider healthSlider;
    
    [HideInInspector] public Animator anim;
    [HideInInspector] public Rigidbody2D rb;

    public string id;
    public float lastJumpPosition;
    public float visionDistance;
    public GameObject followerSpotPrefab;
    public float followDistance = 1;
    public float currentDistance = 0;
    public float leaderSpeed = 5, followerSpeed = 2;
    public float meleeRange = 1.5f, aimFocusThreshold = .5f;

    #region Static functions
    public static void ChangeLayer(Transform parent, string layer)
    {
        parent.gameObject.layer = LayerMask.NameToLayer(layer);
        foreach (Transform child in parent)
        {
            child.gameObject.layer = LayerMask.NameToLayer(layer);
            ChangeLayer(child, layer);
        }
    }
    #endregion

    public CharacterOne InitCharacter(Attributes attr, Gender gender, Role role, LifeCycle age, float maxSpeed, string tribeID, string name)
    {
        attributes = attr;
        attributes.characterName = name;
        attributes.healthPoints = (attr.endurance > 0 ? attr.endurance : 1);
        healthSlider.maxValue = attr.healthPoints;
        this.role = role;
        this.age = age;
        this.tribeID = tribeID;
        this.maxSpeed = maxSpeed;
        this.gender = gender;
        return this;
    }

    public void AddModel(GameObject modelPrefab, Transform tr, Vector3 scale)
    {
        model = Instantiate(modelPrefab, tr);
        model.transform.localScale = scale;
    }
    
    private void Start()
    {
        // Setting up references.
        groundCheck = transform.Find("GroundCheck");
        anim = model.GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        if (attacking)
            LookForHit();
        grounded = false;
        Collider2D[] colliders = Physics2D.OverlapCircleAll(groundCheck.position, groundedRadius, whatIsGround);
        for (int i = 0; i < colliders.Length; i++)
        {
            if (colliders[i].gameObject != gameObject)
                grounded = true;
        }
        
    }

    public void Move(float move, bool jump)
    {
        if (age == LifeCycle.Dead || acted)
            return;

        if (grounded || airControl)
        {
            if (anim)
                anim.SetBool("Walking", (move != 0 ? true : false));
            rb.velocity = new Vector2(move * maxSpeed, rb.velocity.y);
            if (!straff && !GameManager.instance.tribes[tribeID].leader.straff)
            {
                if (move > 0 && !facingRight)
                    Flip();
                else if (move < 0 && facingRight)
                    Flip();
            }
        }
        if (grounded && jump)
        {
            grounded = false;
            rb.AddForce(new Vector2(0f, jumpForce));
        }
    }


    public void Flip()
    {
        facingRight = !facingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    public void ActionInputs(bool m_Melee, bool m_Range, bool m_Pray, bool m_Equipped)
    {
        if (anim)
        {
            MeleeAttack(m_Melee);
            RangeAttack(m_Range);
            Equip(m_Equipped);
            Pray(m_Pray);
        }
    }
    public void LookDown(bool toggle)
    {
        if (!anim)
            anim = model.GetComponent<Animator>();
        anim.SetBool("Down", toggle);
    }

    void MeleeAttack(bool toggle)
    {
        anim.SetBool(MELEE, toggle);
    }

    void RangeAttack(bool toggle)
    {
        anim.SetBool(RANGE, toggle);
    }

    void Equip(bool toggle)
    {
        anim.SetBool(EQUIP, toggle);
    }

    void Pray(bool toggle)
    {
        anim.SetBool(PRAY, toggle);
    }

    public void ResetState()
    {
        range = false;
        equip = false;
        jump = false;
        melee = false;
        order = false;
    }

    public IEnumerator ActionTimer(float timer)
    {
        acted = true;
        bool marker = false;
        if (melee)
        {
            attacking = true;
            marker = true;
        }
        float time = 0;
        while (time < timer)
        {
            time += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        if (marker)
            attacking = false;
        acted = false;
    }

    public void TakeDamage(int damage)
    {
        if (!hit)
        {
            attributes.healthPoints -= damage;
            //Debug.Log(name + " hit for " + damage + " damage");
            if (attributes.healthPoints <= 0)
            {
                Debug.Log(name + " died from attack");
                Die(false);
            }
            StartCoroutine(HitTimer(hitTimer));
            StartCoroutine(ShowHealthBar(hitTimer));
        }
    }

    IEnumerator ShowHealthBar(float showHpTimer)
    {
        float timer = 0;
        healthSlider.value = attributes.healthPoints;
        healthSlider.gameObject.SetActive(true);
        while (timer < showHpTimer)
        {
            timer += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        healthSlider.gameObject.SetActive(false);
    }

    public void Die(bool oldAge)
    {
        if (anim)
            anim.SetBool("Dead", true);
        family.RemoveFamilyLink(this);
        bool isLeader = role == Role.Leader;
        age = LifeCycle.Dead;
        CharacterOne next = null;
        if (isLeader)
            next = DeathOfLeader();
        else
            DeathOfFollower();

        if (!next && isLeader)
            GameOver();
        DeactivateDeadCharacter();
        if (!oldAge)
            RemoveAndDestroy();
    }

    public void RemoveAndDestroy()
    {
        GameManager.instance.tribes[tribeID].RemoveMember(name);
        enabled = false;
        Destroy(gameObject, 2);
    }

   
    void GameOver()
    {
        Debug.Log("No Leader left");
    }

    void DeathOfFollower()
    {
        GameManager.instance.tribes[tribeID].RefreshFollowers();
        GameManager.instance.tribes[tribeID].RemoveFollower(gameObject.name);
    }

    CharacterOne DeathOfLeader()
    {
        GameManager.instance.tribes[tribeID].ToggleEquipFollowers(false);
        CharacterOne next = GameManager.instance.tribes[tribeID].NextLeader();
        if (next)
        {
            GameManager.instance.tribes[tribeID].RemoveFollower(next.name);
            GameManager.instance.tribes[tribeID].AssignTribeLeader(next, true);
        }
        return next;
    }

    IEnumerator HitTimer(float hitTimer)
    {
        hit = true;
        float timer = 0;
        while (timer < hitTimer)
        {
            timer += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        hit = false;
    }

    void LookForHit()
    {
        RaycastHit2D[] hits = Physics2D.CircleCastAll(attackPoint.position, attackRadius, (facingRight ? attackPoint.right : -attackPoint.right), attackRadius, targetMask);
        for (int i = 0; i < hits.Length; i++)
        {
            if (hits[i].collider.gameObject != gameObject && hits[i].collider.transform.parent.tag == "Animal")
            {
                attacking = false;
                hits[i].collider.transform.parent.GetComponent<AnimalAI>().TakeDamage(Random.Range(attackMin, attributes.strength + 1));
            }
        }
    }

    public void AddFollower(CharacterOne c)
    {
        Transform spot = AddTransformToFollow();
        c.target = spot;
    }

    private Transform AddTransformToFollow()
    {
        if (followerSpotPrefab)
        {
            GameObject spot = Instantiate(followerSpotPrefab, transform);
            currentDistance += followDistance;
            spot.transform.localPosition = new Vector3(-currentDistance, 0, 0);
            return spot.transform;
        }
        return null;
    }

    public void DeactivateDeadCharacter()
    {
        GetComponentInChildren<SpriteRenderer>().sortingOrder = 1;
        ChangeLayer(transform, "Default");
        GetComponent<CharacterControl>().enabled = false;
        GetComponent<CharacterAIController>().enabled = false;
    }

    public void ActivateFollowerCharacter()
    {
        GetComponent<CharacterAIController>().enabled = true;
        GetComponent<CharacterControl>().enabled = false;
        GetComponentInChildren<SpriteRenderer>().sortingOrder = 3;
        maxSpeed = followerSpeed;
    }

    public void ActivatePlayerCharacter()
    {
        GetComponent<CharacterControl>().enabled = true;
        GetComponent<CharacterAIController>().enabled = false;
        Camera.main.GetComponent<Camera2DFollow>().target = transform;
        Camera.main.GetComponent<Camera2DFollow>().enabled = true;
        GetComponentInChildren<SpriteRenderer>().sortingOrder = 4;
        maxSpeed = leaderSpeed;
    }
        
}

