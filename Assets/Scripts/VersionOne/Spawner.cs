﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{

    public GameObject prefab;
    public int maxNumber = 3;
    // Use this for initialization
    void Start()
    {
        int num = Random.Range(0, maxNumber);
        for (int i = 0; i < num; i++)
        {
            Instantiate(prefab, transform.position, transform.rotation);
        }
    }
}
	
	
