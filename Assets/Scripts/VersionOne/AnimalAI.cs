﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnimalAI : MonoBehaviour
{

    public Animator m_Anim;
    Rigidbody2D m_Rigidbody2D;
    public float m_MaxSpeed = 3, moveTime = 3, waitTime = 2, attackTime = .5f, fangsAttackRadius = .2f, visionDistance = 2, forceX = 300, forceY = 200, hitTimer = .5f, showHpBarTimer = .5f;
    public Transform attackPoint;
    public bool m_FacingRight, waiting, moving, attacking, preyFound, hit;
    public GameObject prey;
    public int attackMin, attackMax;
    public float attackRate, lastAttackTime;

    public LayerMask preyMask;
    public Slider healthSlider;
    public int maxHealthPoints = 10;
    public int healthPoints;

    // Use this for initialization
    void Start()
    {
        m_Anim = GetComponentInChildren<Animator>();
        m_Rigidbody2D = GetComponent<Rigidbody2D>();
        healthPoints = maxHealthPoints;
        StartCoroutine(MoveTimer(Random.Range(1, moveTime), false));
        healthSlider.maxValue = maxHealthPoints;
    }

    private void FixedUpdate()
    {
        if (m_Anim.GetBool("Attack"))
            m_Anim.SetBool("Attack", false);
    }

    IEnumerator ActionTimer(float actionTimer)
    {
        waiting = true;
        float timer = 0;
        m_Anim.SetBool("Walk", false);
        while (timer < actionTimer)
        {
            timer += Time.deltaTime;
            LookForPrey();
            if (prey)
                timer = actionTimer;
            yield return new WaitForEndOfFrame();
        }
        waiting = false;
        if (!prey)
            NextAction();
        else if (!m_Anim.GetBool("Angry"))
            StartCoroutine(AttackTimer(attackTime, m_FacingRight));
    }

    IEnumerator MoveTimer(float moveTime, bool right)
    {
        moving = true;
        float timer = 0;
        while (timer < moveTime)
        {
            timer += Time.deltaTime;
            Move((right ? 1 : -1));
            yield return new WaitForEndOfFrame();
            LookForPrey();
            if (prey)
                timer = moveTime;
        }
        moving = false;
        if (!prey)
            StartCoroutine(ActionTimer(Random.Range(1, waitTime)));
        else if (!m_Anim.GetBool("Angry"))
            StartCoroutine(AttackTimer(attackTime, m_FacingRight));
    }

    IEnumerator AttackTimer(float attackTime, bool right)
    {
        float timer = 0;
        m_Anim.SetBool("Angry", true);
        while (timer < attackTime)
        {
            timer += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        m_Anim.SetBool("Angry", false);
        HuntingPrey();
        StartCoroutine(MoveTimer(.1f, m_FacingRight));
    }

    void HuntingPrey()
    {
        
        prey = null;
        LookForPrey();
        if (prey && Time.time - lastAttackTime > attackRate && !attacking)
        {
            attacking = true;
            lastAttackTime = Time.time;
            JumpingOnPrey();
        }
        else
            attacking = false;
            
    }

    void JumpingOnPrey()
    {
        m_Rigidbody2D.AddForce(((m_FacingRight ? transform.right : -transform.right) * forceX) + (transform.up * forceY));
        m_Anim.SetBool("Attack", true);
        StartCoroutine(LookForBite());
    }

    IEnumerator LookForBite()
    {
        while (attacking)
        {
            Vector3 direction = (m_FacingRight ? attackPoint.right : -attackPoint.right);
            RaycastHit2D[] hits = Physics2D.CircleCastAll(attackPoint.position, fangsAttackRadius, direction, fangsAttackRadius ,preyMask);
            for (int i = 0; i < hits.Length; i++)
            {
                if (hits[i].collider.gameObject != gameObject && hits[i].collider.transform.parent.tag == "Character")
                {
                    attacking = false;
                    hits[i].collider.transform.parent.GetComponent<CharacterOne>().TakeDamage(Random.Range(attackMin, attackMax + 1));
                    
                }

            }
            yield return new WaitForEndOfFrame();
        }

    }

    void NextAction()
    {
        if (!m_FacingRight)
            StartCoroutine(MoveTimer(Random.Range(1, moveTime), true));
        else
            StartCoroutine(MoveTimer(Random.Range(1, moveTime), false));
    }

    void LookForPrey()
    {
        RaycastHit2D hit = Physics2D.Raycast(attackPoint.position, (m_FacingRight ? attackPoint.right : -attackPoint.right), visionDistance, preyMask);
        if (hit)
            prey = hit.collider.gameObject;
    }

    public void Move(float move)
    {
        if (m_Anim)
            m_Anim.SetBool("Walk", (move != 0 ? true : false));
        m_Rigidbody2D.velocity = new Vector2(move * m_MaxSpeed, m_Rigidbody2D.velocity.y);

        if (move > 0 && !m_FacingRight)
            Flip();

        else if (move < 0 && m_FacingRight)
            Flip();
    }

    private void Flip()
    {
        m_FacingRight = !m_FacingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    public void TakeDamage(int damage)
    {
        if (!hit)
        {
            healthPoints -= damage;
            LookForPrey();
            if (!prey)
                Flip();
            Debug.Log(name + " Took " + damage + " damage");
            if (healthPoints <= 0)
                Die();
            StartCoroutine(HitTimer(hitTimer));
            StartCoroutine(ShowHealthBar(showHpBarTimer));
        }
    }

    IEnumerator HitTimer(float hitTimer)
    {
        hit = true;
        float timer = 0;
        while (timer < hitTimer)
        {
            timer += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        hit = false;
    }

    IEnumerator ShowHealthBar(float showHpTimer)
    {
        float timer = 0;
        healthSlider.value = healthPoints;
        healthSlider.gameObject.SetActive(true);
        while (timer < showHpTimer)
        {
            timer += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        healthSlider.gameObject.SetActive(false);
    }

    void Die()
    {
        GetComponentInChildren<SpriteRenderer>().sortingOrder = 2;
        ChangeLayer(transform, "Default");
        StopAllCoroutines();
        m_Anim.SetBool("Walk", false);
        m_Anim.SetBool("Angry", false);
        m_Anim.SetBool("Attack", false);
        m_Anim.SetBool("Dead", true);
        enabled = false;
    }


    void ChangeLayer(Transform parent, string layer)
    {
        gameObject.layer = LayerMask.NameToLayer(layer);
        foreach (Transform child in parent)
        {
            child.gameObject.layer = LayerMask.NameToLayer(layer);
            ChangeLayer(child, layer);
        }
    }

}
