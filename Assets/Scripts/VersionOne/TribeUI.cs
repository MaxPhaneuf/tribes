﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TribeUI : MonoBehaviour {

    public GameObject[] characterSlots;
    public Tribe tribe;
    public Text tribeName;
    public GameObject mainPanel;
    public Image coverUI;

    public static TribeUI instance;

    private void Awake()
    {
        if (!instance)
            instance = this;
        else
            Destroy(gameObject);
        DontDestroyOnLoad(gameObject);
    }

    public void AssignTribe(Tribe tribe)
    {
        this.tribe = tribe;
        tribeName.text = tribe.tribeName;
        AssignSlots();
    }

    public void AssignSlots()
    {
        if (!tribe || characterSlots.Length == 0)
            return;
        foreach (GameObject o in characterSlots)
            o.SetActive(false);
        int index = 0;
        foreach (CharacterOne member in tribe.members)
        {
            if (member.age != CharacterOne.LifeCycle.Dead)
            {
                Text[] texts = characterSlots[index].GetComponentsInChildren<Text>();
                Image image = characterSlots[index].GetComponentInChildren<Image>();
                texts[0].text = member.attributes.characterName;
                texts[1].text = member.attributes.ToString();
                image.sprite = member.model.GetComponent<SpriteRenderer>().sprite;
                characterSlots[index].SetActive(true);
                if (index < characterSlots.Length)
                    index++;
                else
                    index = 0;
                
            }
        }
    }

    void OnCancelButton()
    {
        mainPanel.SetActive(false);
    }


}
