using UnityEngine;

[RequireComponent(typeof(CharacterOne))]
public class CharacterControl : MonoBehaviour
{
    private CharacterOne character;
    public float meleeTime = .75f;
    public float rangeTime = 1f;
    public float prayTime = .75f;
    public float zoomRate = .1f;

    private void Start()
    {
        character = GetComponent<CharacterOne>();
    }

    private void Update()
    {
        if (!character.jump)
            character.jump = Input.GetButtonDown("Jump");
        if (!character.melee)
            character.melee = Input.GetButtonDown("Fire1");
        if (!character.range)
            character.range = Input.GetButtonDown("Fire2");
        if (!character.order)
            character.order = Input.GetButtonDown("Fire3");
        if (!character.equip)
            character.equip = Input.GetKeyDown(KeyCode.E);
        if (Input.GetKeyDown(KeyCode.Escape) || Input.GetButtonDown("Submit"))
        {
            if (!TribeUI.instance.mainPanel.activeInHierarchy)
                TribeUI.instance.AssignSlots();
            TribeUI.instance.mainPanel.SetActive(!TribeUI.instance.mainPanel.activeInHierarchy);
        }
        if (Input.GetKey(KeyCode.Z) || Input.GetButton("TriggerL"))
        {
            if (Camera.main.orthographicSize > 5)
            {
                Camera.main.gameObject.GetComponent<Camera2DFollow>().OffsetY -= zoomRate;
                Camera.main.orthographicSize -= zoomRate;
            }

        }
            
        if (Input.GetKey(KeyCode.X) || Input.GetButton("TriggerR"))
        {
            if (Camera.main.orthographicSize < 7)
            {
                Camera.main.gameObject.GetComponent<Camera2DFollow>().OffsetY += zoomRate;
                Camera.main.orthographicSize += zoomRate;
            }
        }

        if (Input.GetKeyDown(KeyCode.RightShift) || Input.GetButtonDown("TopTriggerR"))
            character.straff = !character.straff;
    }

    private void FixedUpdate()
    {
        if (!isActiveAndEnabled)
            return;
        float h = Input.GetAxis("Horizontal");

        if (character.equip)
            character.equipped = !character.equipped;

        if (!character.acted)
        {
            if (character.order && !character.equipped)
                character.equipped = true;
            else if ((character.range || character.order) && character.equipped)
                character.equipped = false;
            if (character.melee)
                StartCoroutine(character.ActionTimer(meleeTime));
            if (character.range)
                StartCoroutine(character.ActionTimer(rangeTime));
            if (character.order)
            {
                character.charge = !character.charge;
                GameManager.instance.tribes[character.tribeID].ToggleEquipFollowers(character.charge);
                StartCoroutine(character.ActionTimer(prayTime));
            }
            character.ActionInputs(character.melee, character.range, character.order, character.equipped);
        }

        character.Move(h, character.jump);

        character.ResetState();

    }

    

}

