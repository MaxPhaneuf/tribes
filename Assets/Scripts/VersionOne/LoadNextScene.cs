﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadNextScene : MonoBehaviour
{
    public Transform spawnPoint;
    public bool collided;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!collided)
        {
            if (collision.tag == "Character")
            {
                collided = true;
                if (collision.GetComponentInParent<CharacterControl>().isActiveAndEnabled)
                {
                    string tribeID = collision.GetComponentInParent<CharacterOne>().tribeID;
                    GameManager.instance.ChangeScene(spawnPoint, collision, tribeID);
                }
            }
        }
    }
 }
