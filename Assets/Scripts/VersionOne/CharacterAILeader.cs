﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAILeader : CharacterAIController {

    public GameObject otherCharacter;

    // Use this for initialization
    private void Awake()
    {
        character = GetComponent<CharacterOne>();
    }

     private void Update()
    {
        if (isActiveAndEnabled)
        {
            LookAround();
            if (IsEnemy(otherCharacter))
                character.charge = true;
            else
                character.charge = false;
            if (character.charge)
            {
                LookForEnemy();
                AttackEnemy();
            }
            if (!enemyInSight && character.target)
                MoveAI(character.target.position.x);
        }
        if (character.age == CharacterOne.LifeCycle.Dead)
            StopAllCoroutines();
    }

    void LookAround()
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, (character.facingRight ? transform.right : -transform.right), character.visionDistance, targetMask);
        if (hit)
            otherCharacter = hit.collider.transform.parent.gameObject;
        else
            otherCharacter = null;
    }

    bool IsEnemy(GameObject otherCharacter)
    {
        if (otherCharacter.GetComponent<CharacterOne>() && GameManager.instance.tribes[character.tribeID].alliedTribes.Contains(otherCharacter.GetComponent<CharacterOne>().tribeID))
            return false;
        else
            return true;
    }

    void LookForEnemy()
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, (character.facingRight ? transform.right : -transform.right), character.visionDistance, targetMask);
        if (hit)
            enemyInSight = hit.collider.transform.parent.gameObject;
        else
            enemyInSight = null;
    }

    IEnumerator MotherFleeTimer(float fleeTime)
    {
        float timer = 0;
        //fled = true;
        MotherFlee();
        while (enemyInSight || timer < fleeTime)
        {
            timer += Time.deltaTime;
            LookForEnemy();
            MoveAI(character.target.position.x);
            yield return new WaitForEndOfFrame();
        }
        //fled = false;
        MotherStopFlee();
    }

    void MotherStopFlee()
    {
        character.target.localPosition = new Vector3(lastTargetPos, character.target.localPosition.y, character.target.localPosition.z);
    }

    void MotherFlee()
    {
        lastTargetPos = character.target.localPosition.x;
        float current = GameManager.instance.tribes[character.tribeID].leader.currentDistance;
        character.target.localPosition = new Vector3(-current, character.target.localPosition.y, character.target.localPosition.z);
    }

    private void AttackEnemy()
    {
        if (!character.attacking && enemyInSight)
            StartCoroutine(AttackTimer(attackTime));
    }

    private void MoveAI(float targetPosition)
    {
        float h = 0;
        float tmp = targetPosition - transform.position.x;
        h = Mathf.Clamp(tmp, -1 * character.maxSpeed, 1 * character.maxSpeed);

        if (Mathf.Abs(h) >= OffSetMove)
            character.Move(h, character.jump);
        else
            character.Move(0, character.jump);
    }

    IEnumerator AttackTimer(float timer)
    {
        float time = 0;
        while (enemyInSight && time < timer && !enemyInSight.GetComponentInChildren<Animator>().GetBool("Dead"))
        {
            time += Time.deltaTime;
            bool enemyMoving = Vector2.Distance(enemyInSight.GetComponent<Rigidbody2D>().velocity, Vector2.zero) < character.aimFocusThreshold;
            if (Mathf.Abs(transform.position.x - enemyInSight.transform.position.x) < character.meleeRange && enemyMoving)
            {
                EnemyInRange();
                yield return new WaitForEndOfFrame();
                character.attacking = true;
            }
            else
                EnemyOutOfRange();
            yield return new WaitForEndOfFrame();
        }
        character.attacking = false;
        character.anim.SetBool("Melee", false);
        enemyInSight = null;
    }

    void EnemyInRange()
    {
        character.anim.SetBool("Melee", true);
        character.anim.SetBool("Walking", false);
        if (!character.straff && !GameManager.instance.tribes[character.tribeID].leader.straff)
        {
            if (transform.position.x - enemyInSight.transform.position.x > 0 && character.facingRight)
                character.Flip();
            else if (transform.position.x - enemyInSight.transform.position.x < 0 && !character.facingRight)
                character.Flip();
        }
    }

    void EnemyOutOfRange()
    {
        character.anim.SetBool("Melee", false);
        character.anim.SetBool("Walking", true);
        character.attacking = false;
        float h = 0;
        float tmp = enemyInSight.transform.position.x - transform.position.x;
        h = Mathf.Clamp(tmp, -1 * character.maxSpeed, 1 * character.maxSpeed);

        if (Mathf.Abs(h) >= OffSetMove)
            character.Move(h, character.jump);
        else
            character.Move(0, character.jump);
        
    }

    
}
