﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterGenerator : MonoBehaviour
{
    public GameObject characterPrefab, tribePrefab, mainPanel;
    public GameObject[] maleModels, femaleModels;
    public static CharacterGenerator instance;
    public int points = 30;
    public Text fatherText, motherText, fatherName, motherName;
    public InputField numberChildren, tribeName;
    public Button fatherButton, motherButton;
    public Transform tribeSpawn;
    private CharacterOne.Attributes fatherAtt, motherAtt;
    private string fName, mName;
    public Transform leaderPosition;
    public bool hasMother, hasFather;
    public bool autoGenerate;

    public int motherBirthRateEnd = 5, fatherBirthRateCha = 6;

    public static int currentCharacterID = 0;

    // Use this for initialization
    void Awake()
    {
        if (!instance)
            instance = this;
        else
            Destroy(gameObject);
    }

    private void Start()
    {
        if (autoGenerate)
        {
            CreateTribe(true);
        }
        DontDestroyOnLoad(gameObject);
    }

    # region Attribute Generation Funtions

    public CharacterOne.Attributes GenerateCharacterAttributes(CharacterOne.Attributes father, CharacterOne.Attributes mother)
    {
        CharacterOne.Attributes att = new CharacterOne.Attributes
        {
            strength = Random.Range(GetLowest(father.strength, mother.strength), GetHighest(father.strength, mother.strength) + 1),
            dexterity = Random.Range(GetLowest(father.dexterity, mother.dexterity), GetHighest(father.dexterity, mother.dexterity) + 1),
            intelligence = Random.Range(GetLowest(father.intelligence, mother.intelligence), GetHighest(father.intelligence, mother.intelligence) + 1),
            perception = Random.Range(GetLowest(father.perception, mother.perception), GetHighest(father.perception, mother.perception) + 1),
            endurance = Random.Range(GetLowest(father.endurance, mother.endurance), GetHighest(father.endurance, mother.endurance) + 1),
            charisma = Random.Range(GetLowest(father.charisma, mother.charisma), GetHighest(father.charisma, mother.charisma) + 1)
        };

        return att;
    }

    public CharacterOne.Attributes GenerateCharacterAttributes(int totalPoints)
    {

        int[] tmpList = new int[6];
        int pick;
        if (totalPoints <= 60)
        {
            while (totalPoints > 0)
            {
                pick = Random.Range(0, 6);
                if (tmpList[pick] < 10)
                {
                    tmpList[pick]++;
                    totalPoints--;
                }
            }
        }

        CharacterOne.Attributes att = new CharacterOne.Attributes
        {
            strength = tmpList[0],
            dexterity = tmpList[1],
            intelligence = tmpList[2],
            perception = tmpList[3],
            endurance = tmpList[4],
            charisma = tmpList[5]
        };

        return att;
    }
    
    int GetLowest(int father, int mother)
    {
        if (father <= mother)
            return father;
        else
            return mother;

    }

    int GetHighest(int father, int mother)
    {
        if (father >= mother)
            return father;
        else
            return mother;

    }

    void GenerateCurrentFather()
    {
        fatherAtt = GenerateCharacterAttributes(points);
        fName = NameLibrary.MALE_NAMES[Random.Range(0, NameLibrary.MALE_NAMES.Length)];
    }

    void GenerateCurrentMother()
    {
        motherAtt = GenerateCharacterAttributes(points);
        mName = NameLibrary.FEMALE_NAMES[Random.Range(0, NameLibrary.FEMALE_NAMES.Length)];
    }

#endregion

    #region Create New Character Functions
    public void CreateTribe(bool isPlayer)
    {
        if (tribeName.text == "")
            tribeName.text = "Tribe";
        Tribe currentTribe = Tribe.InitTribe(tribePrefab, tribeName.text);
        if (!hasFather)
            GenerateCurrentFather();
        if (!hasMother)
            GenerateCurrentMother();
        CharacterOne father = CreateFamily(currentTribe);
        currentTribe.AssignTribeLeader(father, isPlayer); // Assign Father as leader of new tribe;
        GameManager.instance.tribes.Add(currentTribe.name, currentTribe);
        TribeUI.instance.AssignTribe(currentTribe);
        if (mainPanel.activeInHierarchy)
            mainPanel.SetActive(false);
    }
    
    CharacterOne CreateFamily(Tribe currentTribe)
    {
        CharacterOne father = CreateFather(fName, fatherAtt, maleModels[0], currentTribe);
        CharacterOne mother = CreateMother(mName, motherAtt, femaleModels[0], currentTribe);
        father.transform.SetParent(currentTribe.transform);
        mother.transform.SetParent(currentTribe.transform);
        CharacterOne.Family parents = new CharacterOne.Family { Father = father, Mother = mother };
        CharacterOne.Family currentFamily = new CharacterOne.Family { children = CreateChildren(PossibleChildren(motherAtt, fatherAtt), parents, currentTribe) };
        father.family = currentFamily;
        mother.family = currentFamily;
        AssignChildrenFamilyMembers(currentFamily, mother, father);
        return father;
    }
        
    CharacterOne CreateFather(string name, CharacterOne.Attributes att, GameObject model, Tribe currentTribe)
    {
        CharacterOne father = CreateMember(name, model, fatherAtt, CharacterOne.Gender.Male, currentTribe.name);
        currentTribe.leader = father;
        AssingCharacterID(father);
        currentTribe.leader = father;
        father.role = CharacterOne.Role.Leader;
        father.age = CharacterOne.LifeCycle.Teen;
        father.model.transform.localScale = new Vector3(1, 1, 1);
        currentTribe.members.Add(father);
        return father;
    }

    CharacterOne CreateMother(string name, CharacterOne.Attributes att, GameObject model, Tribe currentTribe)
    {
        CharacterOne mother = CreateMember(name, model, att, CharacterOne.Gender.Female, currentTribe.name);
        AssingCharacterID(mother);
        mother.maxSpeed = 2;
        currentTribe.followers.Add(mother);
        mother.age = CharacterOne.LifeCycle.Teen;
        mother.model.transform.localScale = new Vector3(1, 1, 1);
        currentTribe.members.Add(mother);
        return mother;
    }

    public List<CharacterOne> CreateChildren(int numberC, CharacterOne.Family parents, Tribe currentTribe)
    {
        CharacterOne.Family currentFamily = new CharacterOne.Family();
        for (int i = 0; i < numberC; i++)
        {
            CharacterOne.Attributes attr = GenerateCharacterAttributes(parents.Father.attributes, parents.Mother.attributes);
            CharacterOne newMember = CreateWithRandomGender(attr, currentTribe);
            newMember.transform.SetParent(currentTribe.transform);
            if (newMember)
            {
                newMember = CharacterAssign(newMember, currentTribe);
                currentFamily.children.Add(newMember);
            }
        }
        return currentFamily.children;
    }

    CharacterOne CreateMember(string name, GameObject model, CharacterOne.Attributes attr, CharacterOne.Gender gender, string tribeID)
    {
        Vector3 pos = new Vector3();
        Quaternion rot = new Quaternion();
        if (GameManager.instance.tribes.ContainsKey(tribeID) && GameManager.instance.tribes[tribeID].leader)
        {
            pos = GameManager.instance.tribes[tribeID].leader.transform.position;
            rot = GameManager.instance.tribes[tribeID].leader.transform.rotation;
        }
        else
        {
            pos = leaderPosition.position;
            rot = leaderPosition.rotation;
        }

        GameObject newMember = Instantiate(characterPrefab, pos, rot);
        CharacterOne c = newMember.GetComponent<CharacterOne>();
        c.InitCharacter(attr, gender, CharacterOne.Role.Member, CharacterOne.LifeCycle.Child, 2, tribeID, name);
        c.AddModel(model, newMember.transform, new Vector3(.8f, .8f, 1));
        return c;
    }

    public CharacterOne CreateByGender(CharacterOne.Gender gender, CharacterOne.Attributes attr, Tribe currentTribe)
    {
        switch (gender)
        {
            case CharacterOne.Gender.Male:
                return CreateMember(NameLibrary.MALE_NAMES[Random.Range(0, NameLibrary.MALE_NAMES.Length)], maleModels[0], attr, CharacterOne.Gender.Male, currentTribe.name);
            case CharacterOne.Gender.Female:
                return CreateMember(NameLibrary.FEMALE_NAMES[Random.Range(0, NameLibrary.FEMALE_NAMES.Length)], femaleModels[0], attr, CharacterOne.Gender.Female, currentTribe.name);
        }
        return null;
    }

    public CharacterOne CreateWithRandomGender(CharacterOne.Attributes attr, Tribe currentTribe)
    {
        int pick = Random.Range(0, 2);
        switch (pick)
        {
            case 0:
                return CreateMember(NameLibrary.MALE_NAMES[Random.Range(0, NameLibrary.MALE_NAMES.Length)], maleModels[0], attr, CharacterOne.Gender.Male, currentTribe.name);
            case 1:
                return CreateMember(NameLibrary.FEMALE_NAMES[Random.Range(0, NameLibrary.FEMALE_NAMES.Length)], femaleModels[0], attr, CharacterOne.Gender.Female, currentTribe.name);
        }
        return null;
    }

    public void CreateNewGeneration(CharacterOne father, CharacterOne mother, Tribe currentTribe)
    {
        CharacterOne.Family parents = new CharacterOne.Family { Father = father, Mother = mother };
        CharacterOne.Family family = new CharacterOne.Family
        {
            children = CreateChildren(PossibleChildren(mother.attributes, father.attributes), parents, GameManager.instance.tribes[father.tribeID])
        };
        AssignChildrenFamilyMembers(family, mother, father);
        foreach(CharacterOne c in family.children)
        {
            if(!father.family.children.Contains(c))
                father.family.children.Add(c);
            if(!mother.family.children.Contains(c))
                mother.family.children.Add(c);
        }
        Debug.Log(father.age.ToString() + " " + father.name + " and " + mother.age.ToString() + " " + mother.name + " had "
                + family.children.Count.ToString() + (family.children.Count > 1 ? " children" : " child"));
           
        currentTribe.AssignNewGenerationToLeader(currentTribe.leader, family.children);
    }

    #endregion

    #region Button Functions
    public void CancelButton()
    {
        gameObject.SetActive(false);
    }

    public void CreateFatherButton()
    {
        fatherAtt = GenerateCharacterAttributes(points);
        fatherText.text = fatherAtt.ToString();
        fatherName.text = NameLibrary.MALE_NAMES[Random.Range(0, NameLibrary.MALE_NAMES.Length)];
        fName = fatherName.text;
        hasFather = true;
    }

    public void CreateMotherButton()
    {
        motherAtt = GenerateCharacterAttributes(points);
        motherText.text = motherAtt.ToString();
        motherName.text = NameLibrary.FEMALE_NAMES[Random.Range(0, NameLibrary.FEMALE_NAMES.Length)];
        mName = motherName.text;
        hasMother = true;
    }
    #endregion

    public int PossibleChildren(CharacterOne.Attributes motherAtt, CharacterOne.Attributes fatherAtt)
    {
        int numberC = 1;
        if (numberChildren.text == "")
        {
            int motherEndurance = (motherAtt.endurance - motherBirthRateEnd > 0 ? motherAtt.endurance - motherBirthRateEnd : 1);
            int fatherCharisma = (fatherAtt.charisma - fatherBirthRateCha > 0 ? fatherAtt.charisma - fatherBirthRateCha : 1);
            numberC = Random.Range(fatherCharisma, motherEndurance + 1);
        }
        else
            numberC = int.Parse(numberChildren.text);
        return numberC;
    }

    public CharacterOne CharacterAssign(CharacterOne newMember, Tribe currentTribe)
    {
        AssingCharacterID(newMember);
        currentTribe.followers.Add(newMember);
        currentTribe.members.Add(newMember);
        return newMember;
    }

    public void AssignChildrenFamilyMembers(CharacterOne.Family currentFamily, CharacterOne mother, CharacterOne father)
    {
        List<CharacterOne> allChildren = new List<CharacterOne>();
        foreach(CharacterOne c in mother.family.children)
        {
            if (!father.family.children.Contains(c))
                allChildren.Add(c);
        }
        allChildren.AddRange(father.family.children);
        allChildren.AddRange(currentFamily.children);
        for (int i = 0; i < allChildren.Count; i++)
        {
            allChildren[i].family = new CharacterOne.Family { Father = father, Mother = mother };
            for (int j = 0; j < allChildren.Count; j++)
            {
                if (allChildren[i] && allChildren[i].name != allChildren[j].name)
                    allChildren[i].family.siblings.Add(allChildren[j]);
            }
        }
    }
   
    void AssingCharacterID(CharacterOne newMember)
    {
        currentCharacterID++;
        newMember.name = newMember.attributes.characterName + " " + currentCharacterID;
    }

    

}
