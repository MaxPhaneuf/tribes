﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Predator : Animal
{
    bool isDay;

    private void Start()
    {
        Anim = Body.GetComponentInChildren<Animator>();
        ToggleView(currentLevel == World.instance.currentLevel);
        isDay = World.instance.isDay;
    }

    void Update()
    {
        if (age != LifeCycle.Dead && states.active && attributes.type != Type.Human)
        {
            if (isDay != World.instance.isDay)
            {
                if (!World.instance.isDay)
                {
                    currentFoodNeed = attributes.foodNeed;
                    lastTimeFed = Time.time;
                }
                isDay = World.instance.isDay;
            }
            if (Anim && states.active && Body.activeInHierarchy)
                AnimationManager();
            TimePassed();
            StateManager();
        }
    }

    public override void AnimationManager()
    {
        base.AnimationManager();
    }

    public override State GetState()
    {
        if (!states.attacked && state != State.Attacking)
        {
            if (currentFoodNeed > 0)
            {
                if (Target)
                    return State.Hunting;
                else if (!Target && attributes.type != Type.Bear)
                {
                    migrationDirection = NextDirection((int)currentLevel.x, (int)currentLevel.y);
                    migrationTargetLvl = NextLvl(migrationDirection);
                    return State.Migrating;
                }
                else if (attributes.type == Type.Bear)
                {
                    if (attributes.function == Function.Hunter)
                        attributes.function = Function.Gatherer;
                    if (attributes.function == Function.Gatherer)
                        attributes.function = Function.Hunter;
                    GetTarget();
                    if (!Target)
                    {
                        migrationDirection = NextDirection((int)currentLevel.x, (int)currentLevel.y);
                        migrationTargetLvl = NextLvl(migrationDirection);
                        return State.Migrating;
                    }
                }
            }
            else if (currentFoodNeed == 0 && currentLevel != originLvl)
            {
                if (originLvl.x > currentLevel.x)
                    migrationDirection = Direction.East;
                else if (originLvl.x < currentLevel.x)
                    migrationDirection = Direction.West;
                migrationTargetLvl = originLvl;
                return State.Migrating;
            }

            if (currentLevel == originLvl)
            {
                GameObject t = LookForPrey();
                if (t)
                {
                    states.attacked = true;
                    Attacker = t;
                }
            }
        }
        else if (states.attacked)
        {
            states.attacked = false;
            return State.Attacking;
        }
        return State.Moving;
    }
}
