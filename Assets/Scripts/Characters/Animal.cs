﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Animal : Character
{
    public Animator Anim;
    public GameObject Target;
    public GameObject Predator, LastPredator;

    public string walk = "Walk", attack = "Attack", thrw = "Throw", angry, dead; //Animation parameter names

    private void Start()
    {
        Anim = Body.GetComponentInChildren<Animator>();
        ToggleView(currentLevel == World.instance.currentLevel);
    }

    void Update()
    {
        if (age != LifeCycle.Dead && states.active && attributes.type != Type.Human && attributes.type != Type.God)
        {
            if (Anim && states.active && Body.activeInHierarchy)
                AnimationManager();
            TimePassed();
            StateManager();
        }
    }

    #region State
    public virtual void StateManager()
    {
        if (!states.waiting && !states.attacked)
        {
            if (state == State.Attacking)
                Attacking(transform.position.x, attributes.moveSpeed);
            else
            {
                Predator = LookForPredator();
                if (Predator && state != State.Flee && !states.fled)
                    StartFlee();
                else if (state == State.Moving)
                    MovingState(AI.targetPosition, transform.position.x, attributes.moveSpeed);
                else if (state == State.Gathering)
                    Gathering(transform.position.x, attributes.moveSpeed);
                else if (state == State.Hunting)
                    Hunting(transform.position.x, attributes.huntSpeed);
                else if (state == State.Flee || state == State.Migrating)
                    ChangingRegion(AI.targetPosition, transform.position.x, attributes.fleeSpeed);
            }
        }
        else if (Time.time - lastTimeWaited >= waitTime || (states.attacked && state != State.Attacking))
            AfterWait();
    }

    public virtual State GetState()
    {
        if (currentFoodNeed > 0 && !states.attacked && state != State.Attacking && !Predator)
        {
            if (attributes.function == Function.Gatherer)
            {
                if (Target && Target.GetComponent<FruitBush>())
                    return State.Gathering;
                else if (!Target)
                    return StartMigration();

            }
            else if (attributes.function == Function.Hunter)
            {
                if (Target)
                    return State.Hunting;
                else if (!Target)
                    return StartMigration();
            }
        }
        else if (states.attacked)
        {
            states.attacked = false;
            return State.Attacking;
        }
        return State.Moving;
    }
    #endregion

    #region Time
    public virtual void TimePassed()
    {
        if (Time.time - lastTimeAged >= attributes.agingRate)
            Aging();
        if (Time.time - lastTimeFed >= attributes.feedingRate)
            Feeding();
        if (currentStarvation >= attributes.starvationRate)
            Hungry();
        if (Time.time - lastTimeMated >= attributes.matingRate && gender == Gender.Female && !states.pregnant)
            Mating();
        if (Time.time - lastTimeHealed >= attributes.healRate && currentHP < attributes.hp)
            Healing();
        if (Time.time - lastSincePregnancy >= attributes.gestationCycle && states.pregnant)
            NewDelivery();
        if (Time.time - lastTimeFled >= fleeCD && states.fled)
            states.fled = false;
    }

    public virtual void Aging()
    {
        age++;
        if (age >= LifeCycle.Dead)
            Dead();
        lastTimeAged = Time.time;
    }

    public void Feeding()
    {
        if (attributes.foodNeed <= currentFoodNeed)
            currentStarvation++;
        else
            currentStarvation = 0;
        if (currentFoodNeed < attributes.foodNeed)
        {
            if (attributes.type < Type.Wolf)
                currentFoodNeed++;
            else
                currentFoodNeed = attributes.foodNeed;
        }
        lastTimeFed = Time.time;

    }

    public void Hungry()
    {
        if (!states.starvation)
            states.starvation = true;
        if (states.starvation)
            TakeDamage(1, gameObject);
        lastTimeStarve = Time.time;
    }

    public void Healing()
    {
        currentHP++;
        lastTimeHealed = Time.time;
        if (currentLevel == World.instance.currentLevel)
            StartCoroutine(ShowHP(1, currentHP));
    }

    public void Mating()
    {
        foreach (Character c in World.instance.levels[(int)currentLevel.y][(int)currentLevel.x].characters)
        {
            if (c.gender == Gender.Male && c.attributes.type == attributes.type)
            {
                states.pregnant = true;
                lastSincePregnancy = Time.time;
                FatherPrefab = c.FatherPrefab;
                return;
            }
        }
        lastTimeMated = Time.time;
    }

    public void NewDelivery()
    {
        World.Options.AnimalSpawner sp = new World.Options.AnimalSpawner();
        prefabs.Add(MotherPrefab);
        sp.groupAmount = 1;
        sp.type = attributes.type;
        int born = attributes.bornRate;
        if (born + World.instance.currentCharacters.TotalCharacter() > World.instance.MaxCharacterPerWorld)
            born = World.instance.MaxCharacterPerWorld - World.instance.currentCharacters.TotalCharacter();
        else if (CurrentByType(World.instance.levels[(int)originLvl.y][(int)originLvl.x].biome, (int)originLvl.x, (int)originLvl.y) + born > MaxByType())
            born = MaxByType() - CurrentByType(World.instance.levels[(int)originLvl.y][(int)originLvl.x].biome, (int)originLvl.x, (int)originLvl.y);
        if (born > 0)
            Babies(born, sp, currentLevel, originLvl);
        states.pregnant = false;
    }
    #endregion

    #region Wait
    public virtual void StartWait(float pivot)
    {
        AI.pivot = pivot;
        lastTimeWaited = Time.time;
        Rb.velocity = new Vector2(0, Rb.velocity.y);
        states.waiting = true;
        states.moving = false;
    }

    public virtual void AfterWait()
    {
        GetTarget();
        Predator = LookForPredator();
        state = GetState();
        StartAIMoveByState();
        states.waiting = false;
    }
    #endregion

    #region Move
    public void Moving(float target, float position, float speed)
    {
        AI.currentMove = GetAICurrentMove(target, position);
        MoveRB(AI.currentMove, Rb, speed);
        states.moving = !OnTarget(target, position, moveThreshold);
    }

    public void MovingState(float target, float position, float speed)
    {
        Moving(target, position, attributes.moveSpeed);
        if (!states.moving)
            StartWait(transform.position.x);
    }

    public void StartAIMoveByState()
    {
        float p = transform.position.x;
        float b = World.instance.levels[(int)currentLevel.y][(int)currentLevel.x].bounds.Right;
        if (state == State.Moving)
            SetAICurrentMove(RandomMove(b), p);
        else if (state == State.Gathering && Target)
            SetAICurrentMove(Target.transform.position.x, p);
        else if (state == State.Hunting && Target)
            SetAICurrentMove(Target.transform.position.x, p);
        else if (state == State.Attacking && Attacker)
            SetAICurrentMove(Attacker.transform.position.x, p);
        else if (state == State.Flee && Predator)
            SetAICurrentMove((migrationDirection == Direction.East || migrationDirection == Direction.South ? b : -b), p);
        else if (state == State.Migrating)
            SetAICurrentMove((migrationDirection == Direction.East || migrationDirection == Direction.South ? b : -b), p);
        states.moving = true;
    }

    public float RandomMove(float b)
    {
        float min = ((AI.pivot - 1) - AI.moveDistance);
        float max = ((AI.pivot + 1) + AI.moveDistance);
        min = (min < -b ? -b : min);
        max = (max > b ? b : max);
        return Random.Range(min, max);
    }

    public float RandomMove(float b, float p)
    {
        float min = ((p - 1) - AI.moveDistance);
        float max = ((p + 1) + AI.moveDistance);
        min = (min < -b ? -b : min);
        max = (max > b ? b : max);
        return Random.Range(min, max);
    }

    void SetAICurrentMove(float t, float p)
    {
        AI.targetPosition = t;
        AI.currentMove = GetAICurrentMove(t, p);
    }

    public float GetAICurrentMove(float target, float position)
    {
        float move = 0;
        if (target < position)
            move = -1;
        else if (target > position)
            move = 1;
        return move;
    }

    public void MoveRB(float velocityX, Rigidbody2D Rb, float speed)
    {
        Rb.velocity = new Vector2(velocityX * speed, Rb.velocity.y);
        if (velocityX > 0 && !states.facingRight)
            Flip();
        else if (velocityX < 0 && states.facingRight)
            Flip();
    }

    public void Flip()
    {
        states.facingRight = !states.facingRight;
        Vector3 tmpScale = transform.localScale;
        tmpScale.x *= -1;
        transform.localScale = tmpScale;
    }
    #endregion

    #region Eat
    void FoodNeedStatusAfterMeal()
    {
        if (currentFoodNeed <= 0)
            currentFoodNeed = 0;
    }

    public void Gathering(float position, float speed)
    {
        if (Target)
            Moving(Target.transform.position.x, position, speed);
        if (!states.moving)
        {
            if (Target && currentFoodNeed > 0)
                EatFruit();
            StartWait(transform.position.x);
        }
    }

    public virtual void Hunting(float position, float speed)
    {
        Target = null;
        GetTarget();
        StartAIMoveByState();
        if (Target)
        {
            if (OnTarget(Target.transform.localPosition.x, transform.localPosition.x, attributes.attackDist))
            {
                if (currentFoodNeed > 0)
                {
                    if (states.meat)
                        EatMeat();
                    else
                        AttackPrey();
                    StartWait((Target ? Target.transform.position.x : transform.position.x));
                }
            }
            else
                Moving(Target.transform.position.x, position, speed);
        }
        else
            StartWait(transform.position.x);
    }

    public void Attacking(float position, float speed)
    {
        StartAIMoveByState();
        if (Attacker && Attacker.GetComponent<Character>().currentLevel == currentLevel && Attacker.GetComponent<Character>().age < LifeCycle.Dead)
        {
            if (OnTarget(Attacker.transform.localPosition.x, transform.localPosition.x, attributes.attackDist))
                CounterAttack();
            else
                Moving(Attacker.transform.position.x, position, speed);
        }
        else
            StartWait(transform.position.x);
    }

    public virtual void EatFruit()
    {
        if (Target.GetComponent<FruitBush>() && Target.GetComponent<FruitBush>().fruits.Count > 0 && !states.eating)
        {
            StartCoroutine(FadeOut(true, Target.GetComponent<FruitBush>().fruits[Target.GetComponent<FruitBush>().fruits.Count - 1].transform, .1f));
            Destroy(Target.GetComponent<FruitBush>().fruits[Target.GetComponent<FruitBush>().fruits.Count - 1], 1);
            Target.GetComponent<FruitBush>().fruits.RemoveAt(Target.GetComponent<FruitBush>().fruits.Count - 1);
            if (currentLevel == World.instance.currentLevel)
                StartCoroutine(HUDIconEffect(World.instance.prefabs.HUDIcons[0]));
            StartCoroutine(Eating(eatingTime));
            currentFoodNeed--;
            FoodNeedStatusAfterMeal();
            lastTimeFed = Time.time;
        }
    }

    public virtual void EatMeat()
    {
        if (Target.GetComponent<Meat>() && Target.GetComponent<Meat>().meatQuantity > 0 && !states.eating)
        {
            Target.GetComponent<Meat>().meatQuantity--;
            if (currentLevel == World.instance.currentLevel)
                StartCoroutine(HUDIconEffect(World.instance.prefabs.HUDIcons[1]));
            StartCoroutine(Eating(eatingTime));
            currentFoodNeed--;
            FoodNeedStatusAfterMeal();
            lastTimeFed = Time.time;
        }
    }

    public virtual void AttackPrey()
    {
        if (ValidPrey())
        {
            if (!states.attacking)
            {
                Rb.AddForce((transform.localScale.x > 0 ? attributes.force : new Vector2(-attributes.force.x, attributes.force.y)), ForceMode2D.Force);
                AttackSoundByType();
                StartCoroutine(LookForHit());
                StartCoroutine(Attacking(attackCD));
            }
            else
                StartWait(transform.position.x);
        }
    }

    public void AttackSoundByType()
    {
        if (attributes.function == Function.Hunter && attributes.type != Type.Human && currentLevel == World.instance.currentLevel)
            GetComponent<AudioSource>().Play();
    }

    public void CounterAttack()
    {
        if (ValidAttacker())
        {
            if (!states.attacking)
            {
                Rb.AddForce((transform.localScale.x > 0 ? attributes.force : new Vector2(-attributes.force.x, attributes.force.y)), ForceMode2D.Force);
                AttackSoundByType();
                StartCoroutine(LookForHitOnAttacker());
                StartCoroutine(Attacking(attackCD));
            }
            else
            {
                states.attacked = true;
                StartWait(transform.position.x);
            }

        }
    }

    public bool ValidPrey()
    {
        return Target.GetComponent<Character>().currentLevel == currentLevel
            && Target.GetComponent<Character>().age < LifeCycle.Dead;
    }

    public bool ValidAttacker()
    {
        return Attacker
            && Attacker.GetComponent<Character>().currentLevel == currentLevel
            && Attacker.GetComponent<Character>().age < LifeCycle.Dead;
    }

    public IEnumerator LookForHit()
    {
        float timer = 0;
        while (timer <= 1 && Target != null && !Target.GetComponent<Character>().states.hit)
        {
            timer += Time.deltaTime;
            if (Target && Mathf.Abs(transform.position.x - Target.transform.position.x) <= hitThreshold)
            {
                if (!Target.GetComponent<Character>().states.hit)
                    TargetHit(Target.GetComponent<Character>());
            }
            yield return new WaitForEndOfFrame();
        }
    }

    IEnumerator LookForHitOnAttacker()
    {
        float timer = 0;
        while (timer <= 1 && Attacker && !Attacker.GetComponent<Character>().states.hit)
        {
            timer += Time.deltaTime;
            if (Attacker && Mathf.Abs(transform.position.x - Attacker.transform.position.x) <= hitThreshold)
            {
                if (!Attacker.GetComponent<Character>().states.hit)
                    TargetHit(Attacker.GetComponent<Character>());
            }
            yield return new WaitForEndOfFrame();
        }
    }

    void TargetHit(Character target)
    {
        Rb.velocity = new Vector2(0, 0);
        if (target.attributes.damage > 0 && target.attributes.type != Type.Human)
            TakeDamage(target.attributes.damage, target.gameObject);
        target.TakeDamage(attributes.damage, gameObject);
    }

    public IEnumerator Eating(float timer)
    {
        states.eating = true;
        yield return new WaitForSecondsRealtime(timer);
        states.eating = false;
    }

    public IEnumerator Attacking(float timer)
    {
        states.attacking = true;
        yield return new WaitForSecondsRealtime(timer);
        states.attacking = false;
    }
    #endregion

    #region Change Region
    public virtual void ChangingRegion(float target, float position, float speed)
    {
        Predator = LookForPredator();
        if (Predator && LastPredator != Predator && state == State.Flee)
            Predator = LastPredator;
        if (Predator || state == State.Migrating)
        {
            if (Predator)
                EscapeOppositeToPredator();
            StartAIMoveByState();
            Moving(target, position, speed);
            if (!states.moving)
            {
                ChangeLevel(NextLvl(migrationDirection));
                states.fled = true;
                lastTimeFled = Time.time;
                if (currentLevel == migrationTargetLvl)
                    StartWait(transform.position.x);
            }
        }
        else
            StartWait(transform.position.x);
    }

    public virtual void StartFlee()
    {
        state = State.Flee;
        LastPredator = Predator;
        states.fled = true;
        lastTimeFled = Time.time;
        EscapeOppositeToPredator();
        StartAIMoveByState();
    }

    public void EscapeOppositeToPredator()
    {
        if (Predator.transform.position.x > transform.position.x)
            migrationDirection = Direction.West;
        else if (Predator.transform.position.x <= transform.position.x)
            migrationDirection = Direction.East;
        migrationDirection = NextDirection((int)currentLevel.x, (int)currentLevel.y);
        migrationTargetLvl = NextLvl(migrationDirection);
    }

    public virtual State StartMigration()
    {
        migrationDirection = NextDirection((int)currentLevel.x, (int)currentLevel.y);
        migrationTargetLvl = NextLvl(migrationDirection);
        return State.Migrating;
    }

    public Vector2 NextLvl(Direction dir)
    {
        float x = currentLevel.x;
        float y = currentLevel.y;
        if (dir == Direction.West)
            return new Vector2(x - 1, y);
        else if (dir == Direction.East)
            return new Vector2(x + 1, y);
        else if (dir == Direction.North)
            return new Vector2(x, y - 1);
        else if (dir == Direction.South)
            return new Vector2(x, y + 1);
        return new Vector2(x, y);

    }

    public void ChangeLevel(Vector2 newDestination)
    {
        World.instance.levels[(int)currentLevel.y][(int)currentLevel.x].characters.Remove(this);
        currentLevel = newDestination;
        World.instance.levels[(int)currentLevel.y][(int)currentLevel.x].characters.Add(this);
        ToggleView(currentLevel == World.instance.currentLevel);
        World.Level.Bounds b = World.instance.levels[(int)currentLevel.y][(int)currentLevel.x].bounds;
        float x = (transform.position.x > 0 ? b.Left : b.Right);
        transform.position = new Vector2(x, transform.position.y);
        if(attributes.function == Function.Gatherer)
            currentFoodNeed++;

    }

    public Direction NextDirection(int x, int y)
    {
        int caveR = World.instance.options.currentWorldInstance.flora.CaveRight;
        int caveL = World.instance.options.currentWorldInstance.flora.CaveLeft;
        //East
        if (migrationDirection == Direction.East && (x + 1 > caveR))
            return Direction.West;
        //West
        else if (migrationDirection == Direction.West && (x - 1 < caveL))
            return Direction.East;
        return migrationDirection;

    }
    #endregion

    #region Targets
    public void GetTarget()
    {
        GameObject prey = null, meat = null;
        if (attributes.function == Function.Gatherer)
            Target = LookFruitBush();
        else if (attributes.function == Function.Hunter)
        {
            float d1 = float.MaxValue;
            float d2 = float.MaxValue;
            meat = LookForMeat();
            prey = LookForPrey();
            if (meat)
                d1 = Mathf.Abs(transform.position.x - meat.transform.position.x);
            if (prey)
                d2 = Mathf.Abs(transform.position.x - prey.transform.position.x);
            if (meat && d1 < d2)
            {
                Target = meat;
                states.meat = true;
            }
            if (d2 < d1 && prey)
            {
                Target = prey;
                states.meat = false;
            }
        }

    }

    GameObject LookFruitBush()
    {
        float dist = float.MaxValue;
        GameObject tmp = null;
        foreach (FruitBush f in World.instance.levels[(int)currentLevel.y][(int)currentLevel.x].foodSources)
        {
            float d = Mathf.Abs(transform.position.x - f.transform.position.x);
            if (d <= dist && f.fruits.Count > 0 && f.currentLevel == currentLevel)
            {
                dist = d;
                tmp = f.gameObject;
            }
        }
        return tmp;
    }

    public GameObject LookForMeat()
    {
        float dist = float.MaxValue;
        GameObject tmp = null;

        foreach (Meat m in World.instance.levels[(int)currentLevel.y][(int)currentLevel.x].meats)
        {
            if (m)
            {
                float d = Mathf.Abs(transform.position.x - m.transform.position.x);
                if (d <= dist && m.currentLevel == currentLevel)
                {
                    dist = d;
                    tmp = m.gameObject;
                }
            }
        }
        return tmp;
    }

    public GameObject LookForPrey()
    {
        float dist = float.MaxValue;
        GameObject tmp = null;


        foreach (Character c in World.instance.levels[(int)currentLevel.y][(int)currentLevel.x].characters)
        {
            if (c)
            {
                float d = Mathf.Abs(transform.position.x - c.transform.position.x);
                if (d <= dist
                    && (c.attributes.type < attributes.type || (attributes.type == Type.Human && c.attributes.type != Type.Human))
                    && c.age < LifeCycle.Dead
                    && c.currentLevel == currentLevel
                    && c.gameObject.activeInHierarchy)
                {
                    dist = d;
                    tmp = c.gameObject;
                }
            }
        }
        return tmp;
    }

    public virtual GameObject LookForPredator()
    {
        GameObject tmp = null;
        float d = float.MaxValue;
        foreach (Character c in World.instance.levels[(int)currentLevel.y][(int)currentLevel.x].characters)
        {
            float dist = float.MaxValue;
            if (c)
                dist = Mathf.Abs(transform.position.x - c.transform.position.x);
            if (c && dist <= attributes.visionRange
                && c.attributes.function == Function.Hunter
                && (c.attributes.type > attributes.type || (c.attributes.type == Type.Human && attributes.function == Function.Gatherer))
                && c.currentLevel == currentLevel && c.age < LifeCycle.Dead
                && (c.state == State.Hunting || c.state == State.Attacking || (c.attributes.type == Type.Human && c.state == State.Controlled)))
            {
                if (dist < d)
                {
                    tmp = c.gameObject;
                    d = dist;
                }
            }
        }
        return tmp;
    }

    public bool OnTarget(float target, float position, float threshold)
    {
        return Mathf.Abs(target - position) <= threshold;
    }
    #endregion

    #region Other
    int MaxByType()
    {
        if (attributes.type == Type.Frog)
            return World.instance.MaxFrog;
        else if (attributes.type == Type.Rabbit)
            return World.instance.MaxRabbit;
        else if (attributes.type == Type.Snake)
            return World.instance.MaxSnake;
        else if (attributes.type == Type.Goat)
            return World.instance.MaxGoat;
        else if (attributes.type == Type.Dear)
            return World.instance.MaxDear;
        else if (attributes.type == Type.Wolf)
            return World.instance.MaxWolf;
        else if (attributes.type == Type.Bear)
            return World.instance.MaxBear;
        return 0;
    }

    int CurrentByType()
    {
        if (attributes.type == Type.Frog)
            return World.instance.currentCharacters.Frog;
        else if (attributes.type == Type.Rabbit)
            return World.instance.currentCharacters.Rabbit;
        else if (attributes.type == Type.Snake)
            return World.instance.currentCharacters.Snake;
        else if (attributes.type == Type.Goat)
            return World.instance.currentCharacters.Goat;
        else if (attributes.type == Type.Dear)
            return World.instance.currentCharacters.Dear;
        else if (attributes.type == Type.Wolf)
            return World.instance.currentCharacters.Wolf;
        else if (attributes.type == Type.Bear)
            return World.instance.currentCharacters.Bear;
        return 0;
    }

    int CurrentByType(World.Biome biome, int x, int y)
    {

        if (biome == World.Biome.SpringForest)
        {
            if (attributes.type == Type.Frog)
                return World.instance.currentCharacters.charPerBiome.springForest.Frog;
            else if (attributes.type == Type.Rabbit)
                return World.instance.currentCharacters.charPerBiome.springForest.Rabbit;
            else if (attributes.type == Type.Snake)
                return World.instance.currentCharacters.charPerBiome.springForest.Snake;
            else if (attributes.type == Type.Goat)
                return World.instance.currentCharacters.charPerBiome.springForest.Goat;
            else if (attributes.type == Type.Dear)
                return World.instance.currentCharacters.charPerBiome.springForest.Dear;
            else if (attributes.type == Type.Wolf)
                return World.instance.currentCharacters.charPerBiome.springForest.Wolf;
            else if (attributes.type == Type.Bear)
                return World.instance.currentCharacters.charPerBiome.springForest.Bear;
        }
        else if (biome == World.Biome.PineForest)
        {
            if (attributes.type == Type.Frog)
                return World.instance.currentCharacters.charPerBiome.pineForest.Frog;
            else if (attributes.type == Type.Rabbit)
                return World.instance.currentCharacters.charPerBiome.pineForest.Rabbit;
            else if (attributes.type == Type.Snake)
                return World.instance.currentCharacters.charPerBiome.pineForest.Snake;
            else if (attributes.type == Type.Goat)
                return World.instance.currentCharacters.charPerBiome.pineForest.Goat;
            else if (attributes.type == Type.Dear)
                return World.instance.currentCharacters.charPerBiome.pineForest.Dear;
            else if (attributes.type == Type.Wolf)
                return World.instance.currentCharacters.charPerBiome.pineForest.Wolf;
            else if (attributes.type == Type.Bear)
                return World.instance.currentCharacters.charPerBiome.pineForest.Bear;
        }
        else if (biome == World.Biome.NarrowForest)
        {
            if (attributes.type == Type.Frog)
                return World.instance.currentCharacters.charPerBiome.narrowForest.Frog;
            else if (attributes.type == Type.Rabbit)
                return World.instance.currentCharacters.charPerBiome.narrowForest.Rabbit;
            else if (attributes.type == Type.Snake)
                return World.instance.currentCharacters.charPerBiome.narrowForest.Snake;
            else if (attributes.type == Type.Goat)
                return World.instance.currentCharacters.charPerBiome.narrowForest.Goat;
            else if (attributes.type == Type.Dear)
                return World.instance.currentCharacters.charPerBiome.narrowForest.Dear;
            else if (attributes.type == Type.Wolf)
                return World.instance.currentCharacters.charPerBiome.narrowForest.Wolf;
            else if (attributes.type == Type.Bear)
                return World.instance.currentCharacters.charPerBiome.narrowForest.Bear;
        }
        else if (biome == World.Biome.Cave)
        {
            x = (x == World.instance.options.currentWorldInstance.flora.CaveRight ? 1 : 0);
            if (attributes.type == Type.Frog)
                return World.instance.currentCharacters.charPerBiome.caves.indexes[y][x].Frog;
            else if (attributes.type == Type.Rabbit)
                return World.instance.currentCharacters.charPerBiome.caves.indexes[y][x].Rabbit;
            else if (attributes.type == Type.Snake)
                return World.instance.currentCharacters.charPerBiome.caves.indexes[y][x].Snake;
            else if (attributes.type == Type.Goat)
                return World.instance.currentCharacters.charPerBiome.caves.indexes[y][x].Goat;
            else if (attributes.type == Type.Dear)
                return World.instance.currentCharacters.charPerBiome.caves.indexes[y][x].Dear;
            else if (attributes.type == Type.Wolf)
                return World.instance.currentCharacters.charPerBiome.caves.indexes[y][x].Wolf;
            else if (attributes.type == Type.Bear)
                return World.instance.currentCharacters.charPerBiome.caves.indexes[y][x].Bear;

        }
        return 0;
    }

    public virtual void AnimationManager()
    {
        if (walk != "")
            Anim.SetBool(walk, states.moving);
    }
    #endregion
}
