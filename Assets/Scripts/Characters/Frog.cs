﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Frog : Animal {

    private void Start()
    {
        ToggleView(currentLevel == World.instance.currentLevel);
    }

    void Update()
    {
        if (age != LifeCycle.Dead && states.active && attributes.type != Type.Human)
        {
            if (Anim && states.active && Body.activeInHierarchy)
                AnimationManager();
            TimePassed();
            StateManager();
        }
    }

    public override void StateManager()
    {
        if (!states.waiting && !states.attacked)
        {
            if (state == State.Attacking)
                Attacking(transform.position.x, attributes.moveSpeed);
            else
            {
                Predator = LookForPredator();
                if (Predator && state != State.Flee && !states.fled)
                    StartFlee();
                else if (state == State.Moving)
                    MovingState(AI.targetPosition, transform.position.x, attributes.moveSpeed);
                else if (state == State.Gathering)
                    Gathering(transform.position.x, attributes.moveSpeed);
                else if (state == State.Hunting)
                    Hunting(transform.position.x, attributes.moveSpeed);
                else if (state == State.Flee || state == State.Migrating)
                    ChangingRegion(AI.targetPosition, transform.position.x, attributes.moveSpeed);
            }
        }
        else if (Time.time - lastTimeWaited >= waitTime || (states.attacked && state != State.Attacking))
            AfterWait();
    }

    public override void TimePassed()
    {
        if (Time.time - lastTimeAged >= attributes.agingRate)
            Aging();
        if (Time.time - lastTimeMated >= attributes.matingRate && gender == Gender.Female && !states.pregnant)
            Mating();
        if (Time.time - lastTimeHealed >= attributes.healRate && currentHP < attributes.hp)
            Healing();
        if (Time.time - lastSincePregnancy >= attributes.gestationCycle && states.pregnant)
            NewDelivery();
    }

    public override void AnimationManager()
    {
        base.AnimationManager();
    }
}
