﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Character : MonoBehaviour
{
    public const string BODY = "Body";
    public const int FLASH_AMOUNT = 5;
    public const float FLASH_TIME = .05f;
    public const float SHOW_HP_TIME = .5f;

    public enum Type { Human, Frog, Rabbit, Bird, Snake, Goat, Dear, Wolf, Bear, God }
    public enum Function { Gatherer, Hunter }
    public enum LifeCycle { Child, Teen, Adult, Old, Dead }
    public enum Gender { Male, Female }
    public enum State { Moving, Hunting, Gathering, Migrating, Flee, Attacking, Controlled }
    public enum Direction { North, South, East, West }

    [System.Serializable]
    public struct Attributes
    {
        public string name;
        public Type type;
        public Function function;
        public int agingRate; // Number of season for Life cycle change
        public int starvationRate; //Number of season can go without food
        public int feedingRate;
        public int matingRate;
        public int healRate;
        public int foodNeed; //Number of food needed per season to avoid starvation
        public int foodValue; //Food earned by hunting;
        public int hp, damage, reduction, accuracy;
        public float visionRange,attackDist,attackCD, hitCD;
        public float moveSpeed, huntSpeed, fleeSpeed;
        public int gestationCycle; //Duration of pregnacy for female
        public int bornRate; //Number of maximum possible child per pregnacy
        public float HUDPosition; //Position of heads-up display
        public Vector2 force;
    }
    [System.Serializable]
    public struct States
    {
        public bool facingRight;
        public bool pregnant, starvation;
        public bool moving, waiting, active, attacking, eating, fading, hit, cooldown, meat, attacked, fled;
    }
    [System.Serializable]
    public struct AIAttributes
    {
        public float moveDistance; //Distance this character will move from pivot
        public float pivot; // Center of this animal movement
        public float targetPosition, currentMove;
    }

    public State state;
    public LifeCycle age;
    public Gender gender;
    public Direction migrationDirection;
    public GameObject Model, Body, Attacker;
    public Transform Health;
    public RectTransform HUD;
    public Rigidbody2D Rb { get { return GetComponent<Rigidbody2D>(); } }
    public Vector2 lastLevel;
    public Vector2 currentLevel;
    public Vector2 migrationTargetLvl;
    public Vector2 originLvl;
    public Attributes attributes;
    public AIAttributes AI;
    public States states;
    public int currentStarvation;
    public int currentFoodNeed;
    public int currentGestation;
    public int currentHP;
    public float lastTimeAged, lastTimeStarve, lastTimeMated, lastTimeHealed, lastSincePregnancy, lastTimeFed, lastTimeWaited, lastTimeFled;
    public static int ID;
    public int agingDiversity = 10;
    public float hitThreshold = 1, moveThreshold = 1, attackCD = 1, fleeCD = 1;
    public float waitTime = 1f, eatingTime = 1;
    public GameObject FatherPrefab, MotherPrefab, currentPrefab;
    public List<GameObject> prefabs = new List<GameObject>();
    
    #region Activation
    public void Activate()
    {
        states.facingRight = true;
        states.waiting = true;
        int perCent = (int)((attributes.agingRate / 100f) * agingDiversity);
        attributes.agingRate = Random.Range(attributes.agingRate - perCent, (attributes.agingRate + 1) + perCent);
        attributes.gestationCycle = Random.Range(attributes.gestationCycle - perCent, (attributes.gestationCycle + 1) + perCent);
        if (attributes.type != Type.Frog)
            attributes.feedingRate = Random.Range(attributes.feedingRate - perCent, (attributes.feedingRate + 1) + perCent);
        name = attributes.type.ToString() + " " + ID++;
        HUD = transform.Find("HUD").GetComponent<RectTransform>();
        HUD.localPosition = new Vector2(HUD.localPosition.x, attributes.HUDPosition);
        Health = HUD.Find("Health");
        for (int i = 0; i < 5; i++)
        {
            GameObject obj = Instantiate(World.instance.prefabs.HUDIcons[2], Health);
            obj.SetActive(false);
        }
        Model = transform.Find("Model").gameObject;
        Body = Model.transform.Find(BODY).gameObject;
        Body.SetActive(true);
        currentHP = attributes.hp;
        lastTimeAged = Time.time;
        lastTimeHealed = Time.time;
        lastTimeMated = Time.time;
        lastTimeWaited = Time.time;
        lastTimeFed = Time.time;
        lastTimeStarve = Time.time;
        lastTimeFled = Time.time;
        migrationTargetLvl = originLvl;
        waitTime = Random.Range(waitTime - .1f, waitTime + .1f);
        states.active = true;
        ToggleView(currentLevel == World.instance.currentLevel);
    }
    #endregion

    #region Render
    public virtual void ToggleView(bool toggle)
    {
        Model.transform.Find(BODY).GetComponentInChildren<SpriteRenderer>().color = (toggle ? new Color(1, 1, 1, 1) : new Color(1, 1, 1, 0));
    }
    #endregion

    #region Damage
    public void TakeDamage(int dam, GameObject sender)
    {
        if (!states.hit)
        {
            if (sender.GetComponent<Character>().attributes.type == Type.Human && attributes.type != Type.Human && attributes.function == Function.Hunter)
            {
                Attacker = sender;
                states.attacked = true;
            }
            lastTimeHealed = Time.time;
            currentHP -= dam;
            StartCoroutine(WaitHitCD(attributes.hitCD));
            if (currentHP <= 0)
                Dead();
            else if (currentLevel == World.instance.currentLevel)
            {
                StartCoroutine(QuickFlash(FLASH_AMOUNT, FLASH_TIME));
                StartCoroutine(ShowHP(SHOW_HP_TIME, currentHP));
            }
        }

    }

    IEnumerator WaitHitCD(float time)
    {
        states.hit = true;
        yield return new WaitForSecondsRealtime(time);
        states.hit = false;
    }
    #endregion

    #region CreateAndDestroy
    public void Babies(int born, World.Options.AnimalSpawner sp, Vector2 lvl, Vector2 origin)
    {
        int n = Random.Range(0, 2);
        bool female = n == 0;
        for (int i = 0; i < born; i++)
        {
            if (!female)
                sp.males++;
            else
                sp.females++;
            female = !female;

        }
        World.instance.SpawnerWithMother(sp, transform, lvl, origin);
    }

    public void Babies(int born, World.Options.AnimalSpawner sp)
    {
        int n = Random.Range(0, 2);
        bool female = n == 0;
        for (int i = 0; i < born; i++)
        {
            if (!female)
                sp.males++;
            else
                sp.females++;
            female = !female;

        }
        World.instance.SpawnerWithOrigin(sp, this, originLvl);
    }

    public void Dead()
    {
        age = LifeCycle.Dead;
        states.active = false;
        SpawnMeat(attributes.foodValue);
        World.instance.levels[(int)currentLevel.y][(int)currentLevel.x].characters.Remove(this);
        if (currentLevel == World.instance.currentLevel && Body.activeInHierarchy)
        {
            StartCoroutine(HUDIconEffect(World.instance.prefabs.HUDIcons[3]));
            StartCoroutine(FadeOut(true, Body.transform.GetChild(0), .1f));
            if (attributes.type == Type.Human && gameObject.activeInHierarchy)
            {
                StartCoroutine(FadeOut(true, GetComponent<Human>().HairAnim.transform, .1f));
                if(GetComponent<Human>().facialAnimObj && GetComponent<Human>().facialAnimObj.activeInHierarchy)
                    StartCoroutine(FadeOut(true, GetComponent<Human>().FacialAnim.transform, .1f));
                if (GetComponent<Human>().weaponAnimObj && GetComponent<Human>().weaponAnimObj.activeInHierarchy)
                    StartCoroutine(FadeOut(true, GetComponent<Human>().WeaponAnim.transform, .1f));
            }

        }
        Destroy(gameObject, 2);
    }

    private void OnDestroy()
    {
        if (attributes.type != Type.Human)
        {
            if (World.instance)
            {
                World.instance.CurrentType(this, true);
                World.instance.CurrentType(this, World.instance.levels[(int)originLvl.y][(int)originLvl.x].biome, (int)originLvl.x, (int)originLvl.y, true);
                if (World.instance.started)
                {
                    if (World.instance.CountByType(attributes.type, World.instance.levels[(int)originLvl.y][(int)originLvl.x].biome, (int)originLvl.x, (int)originLvl.y) <= 0)
                    {
                        World.Options.AnimalSpawner sp = new World.Options.AnimalSpawner
                        {
                            type = attributes.type,
                            groupAmount = 1,
                        };
                        Babies(2, sp);
                    }
                }
            }
        }
    }

    public void SpawnMeat(int foodValue)
    {
        GameObject obj = Instantiate(World.instance.prefabs.Meat, World.instance.meat);
        obj.transform.position = transform.position;
        obj.GetComponent<Meat>().currentLevel = currentLevel;
        obj.GetComponent<Meat>().meatQuantity = foodValue;
        World.instance.levels[(int)currentLevel.y][(int)currentLevel.x].meats.Add(obj.GetComponent<Meat>());
        if (currentLevel == World.instance.currentLevel)
            obj.GetComponent<Meat>().body.SetActive(true);
    }
    #endregion

    #region HUD Functions
    public IEnumerator HUDIconEffect(GameObject icon)
    {
        GameObject tmp = Instantiate(icon, HUD);
        while (tmp.transform.localPosition.y < World.instance.iconEffectY)
        {
            Vector2 newPos = new Vector2(tmp.transform.localPosition.x, tmp.transform.localPosition.y + (Time.deltaTime * World.instance.iconEffectSpeed));
            tmp.transform.localPosition = newPos;
            yield return new WaitForEndOfFrame();
        }
        Destroy(tmp);
    }

    public IEnumerator FadeOut(bool isFade, Transform tr, float speed)
    {
        states.fading = isFade;
        float a = (isFade ? 0 : 1);
        Color target = new Color(1, 1, 1, a);
        SpriteRenderer spr = tr.GetComponent<SpriteRenderer>();
        while (spr && Mathf.Abs(spr.color.a - target.a) > 0.1f)
        {
            spr.color = Color.Lerp(spr.color, target, speed);
            yield return new WaitForEndOfFrame();
        }
        if(spr)
            spr.color = target;
        states.fading = !isFade;
    }

    public IEnumerator QuickFlash(int n, float speed)
    {
        for (int i = 0; i < n && age < LifeCycle.Dead; i++)
        {
            ToggleView(false);
            yield return new WaitForSecondsRealtime(speed);
            ToggleView(true);
            yield return new WaitForSecondsRealtime(speed);
        }
    }

    public IEnumerator ShowHP(float time, int hp)
    {
        Health.gameObject.SetActive(true);
        for (int i = 0; i < hp; i++)
        {
            Health.GetChild(i).gameObject.SetActive(true);
        }
        yield return new WaitForSecondsRealtime(time);
        for (int i = 0; i < hp; i++)
        {
            Health.GetChild(i).gameObject.SetActive(false);
        }
        Health.gameObject.SetActive(false);
    }
    #endregion
}
