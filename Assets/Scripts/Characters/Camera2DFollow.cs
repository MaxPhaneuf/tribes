using System;
using UnityEngine;

public class Camera2DFollow : MonoBehaviour
{
    public Transform target;
    public float damping = 1;
    public float lookAheadFactor = 3;
    public float lookAheadReturnSpeed = 0.5f;
    public float lookAheadMoveThreshold = 0.1f;
    public float OffsetY = 4.8f;
    public Transform boundLeft, boundRight;

    public float minBound, maxBound;
    private float m_OffsetZ;
    public float offSetX = 5;
    private Vector3 m_LastTargetPosition;
    private Vector3 m_CurrentVelocity;
    private Vector3 m_LookAheadPos;

    public static Camera2DFollow instance;

    // Use this for initialization
    private void Start()
    {
        if (!instance)
            instance = this;
        //m_LastTargetPosition = target.position;
        m_OffsetZ = -10;//(transform.position - target.position).z;
        transform.parent = null;
    }

    public void InitCam()
    {
        minBound = World.instance.levels[(int)World.instance.currentLevel.y][(int)World.instance.currentLevel.x].bounds.Left + offSetX;
        maxBound = World.instance.levels[(int)World.instance.currentLevel.y][(int)World.instance.currentLevel.x].bounds.Right - offSetX;
    }

    // Update is called once per frame
    private void Update()
    {
        if (target)
        {
            Vector3 position = new Vector3(target.position.x, 0, target.position.z);
            // only update lookahead pos if accelerating or changed direction
            if (position.x < minBound)
            {
                position = new Vector3(minBound, 0, target.position.z);
            }
            else if(position.x > maxBound)
            {
                position = new Vector3(maxBound, 0, target.position.z);
            }
            float xMoveDelta = (position - m_LastTargetPosition).x;
            bool updateLookAheadTarget = Mathf.Abs(xMoveDelta) > lookAheadMoveThreshold;
            if (updateLookAheadTarget)
            {
                m_LookAheadPos = lookAheadFactor * Vector3.right * Mathf.Sign(xMoveDelta);
            }
            else
            {
                m_LookAheadPos = Vector3.MoveTowards(m_LookAheadPos, Vector3.zero, Time.deltaTime * lookAheadReturnSpeed);
            }

            Vector3 aheadTargetPos = position + m_LookAheadPos + Vector3.forward * m_OffsetZ; //+ new Vector3(0, OffsetY, 0);
            Vector3 newPos = Vector3.SmoothDamp(transform.position, aheadTargetPos, ref m_CurrentVelocity, damping);

            transform.position = newPos;

            m_LastTargetPosition = position;

        }
    }

    public void ChangeBounds(float min, float max)
    {
        minBound = min + offSetX;
        maxBound = max - offSetX;
    }

    public void AdjustCameraPosition()
    {
        if (transform.position.x > maxBound)
            transform.position = new Vector3(maxBound, transform.position.y, transform.position.z);
        else if (transform.position.x < minBound)
           transform.position = new Vector3(minBound, transform.position.y, transform.position.z);
    }
}

