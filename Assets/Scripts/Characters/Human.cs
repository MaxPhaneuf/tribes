﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Human : Animal
{
    [System.Serializable]
    public class Tribe
    {
        public string name;
        public List<Human> followers = new List<Human>();
        public Human leader;
        public Vector2 currentLevel;
    }
    public GameObject Hair, Facial, Weapon;
    public GameObject facialAnimObj, weaponAnimObj, hairAnimObj;
    public Animator HairAnim { get { return Hair.GetComponentInChildren<Animator>(); } }
    public Animator FacialAnim { get { return Facial.GetComponentInChildren<Animator>(); } }
    public Animator WeaponAnim { get { return Weapon.GetComponentInChildren<Animator>(); } }
    public bool range, melee, cooldown, feed, control, thrown, playerControl, AIThrow;
    public bool currentMelee, currentRange;
    public GameObject spearTip;
    public Transform currentSpear;
    public Meat meat;
    public FruitBush bush;
    public Vector2 initSpearPos, rangeRecoil, meleeRecoil;
    public int currentFoodHeld;
    public Tribe currentTribe;
    public float currentLeaderPos;
    public float lastTimeAttacked, lastTimePlayed, idleTime, throwDist;
    void Start()
    {
        currentLevel = World.instance.currentLevel;
        migrationDirection = Direction.West;
        spearTip.SetActive(false);
        lastTimePlayed = Time.time;
    }

    void Update()
    {
        if (age != LifeCycle.Dead && states.active)
        {
            if (playerControl && currentTribe.leader && currentTribe.leader.name == name)
            {
                if ((IsInput() && state != State.Controlled))
                {
                    state = State.Controlled;
                    lastTimePlayed = Time.time;
                }
            }
            if (state == State.Controlled)
                PlayerUpdateLoop();
            else
                AIUpdateLoop();

        }
    }

    bool IsInput()
    {
        return Input.anyKey
            || Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0
            || Input.GetButton("Fire1") || Input.GetButton("Fire2") || Input.GetButton("Fire3") || Input.GetButton("Jump");
    }

    #region Loop
    void PlayerUpdateLoop()
    {
        if (World.instance.started && !World.instance.debug && !World.instance.busy && !UIManager.instance.covered && !control)
        {
            if (age >= LifeCycle.Dead)
                UIManager.instance.DebugMode();

            if (!cooldown)
                GetActions();
            AnimationManager();
            TimePassed();
            ChangeRegion(transform.position.x);
        }
    }

    void ChangeRegion(float x)
    {
        float b1 = World.instance.levels[(int)currentLevel.y][(int)currentLevel.x].bounds.Right;
        float b2 = World.instance.levels[(int)currentLevel.y][(int)currentLevel.x].bounds.Left;
        if ((x > b1 && currentLevel.x < World.instance.options.currentWorldInstance.sizeX) || (x < b2 && currentLevel.x > 0))
        {
            Vector2 lvl = (x > b1 ? new Vector2(currentLevel.x + 1, currentLevel.y) : new Vector2(currentLevel.x - 1, currentLevel.y));
            World.instance.StartCoroutine(World.instance.MoveToLevel(lvl));
            migrationDirection = (x > b1 ? Direction.East : Direction.West);
        }

    }
    void AIUpdateLoop()
    {
        if (states.active && !control)
        {
            AnimationManager();
            StateManager();
            TimePassed();
        }
    }
    #endregion

    #region TimeAndState
    public override void TimePassed()
    {
        if (Time.time - lastTimeFed >= attributes.feedingRate)
            Feeding();
        if (currentStarvation >= attributes.starvationRate)
            Hungry();
        if (Time.time - lastTimeHealed >= attributes.healRate && currentHP < attributes.hp)
            Healing();
        if (Time.time - lastTimeFled >= fleeCD && states.fled)
            states.fled = false;
        if (Time.time - lastTimePlayed >= idleTime && state == State.Controlled)
        {
            state = State.Moving;
            StartWait(transform.position.x);
        }
        if (Time.time - lastTimeAttacked >= attackCD && cooldown)
            ResetActions();
    }

    public override void StateManager()
    {
        if (!states.waiting)
        {
            Predator = LookForPredator();
            if (Predator && LastPredator != Predator && state == State.Flee)
                Predator = LastPredator;
            //With update loop for live position update
            if (state != State.Migrating && state != State.Flee && currentTribe.leader
                && currentTribe.leader.name != name && currentTribe.leader.currentLevel == currentLevel
                && Mathf.Abs(currentLeaderPos - currentTribe.leader.transform.position.x) >= moveThreshold)
            {
                currentLeaderPos = currentTribe.leader.transform.position.x;
                AI.targetPosition = RandomMove(World.instance.levels[(int)currentLevel.y][(int)currentLevel.x].bounds.Right, currentTribe.leader.transform.position.x);
            }
            if (currentTribe.leader && currentTribe.leader.currentLevel != currentLevel && state != State.Migrating)
            {
                migrationDirection = currentTribe.leader.migrationDirection;
                state = StartMigration();
                StartAIMoveByState();
            }
            else if (state == State.Flee || state == State.Migrating)
                ChangingRegion(AI.targetPosition, transform.position.x, attributes.moveSpeed);
            else if (currentTribe.leader.currentLevel == currentLevel)
            {
                if (Predator && state != State.Flee && !states.fled)
                    StartFlee();
                if (state == State.Moving)
                    MovingState(AI.targetPosition, transform.position.x, attributes.moveSpeed);
                else if (state == State.Gathering && currentFoodNeed > 0)
                    Gathering(transform.position.x, attributes.moveSpeed);
                else if (state == State.Hunting && currentFoodNeed > 0)
                {
                    if (Target && Mathf.Abs(transform.position.x - Target.transform.position.x) >= throwDist && !thrown)
                        AIThrow = false;
                    Hunting(transform.position.x, attributes.moveSpeed);
                }
            }
        }
        else if (Time.time - lastTimeWaited >= waitTime)
            AfterWait();
    }

    public override State GetState()
    {
        if (currentFoodNeed > 0)
        {
            if (Target) {
                if (attributes.function == Function.Gatherer)
                {
                    if (Target.GetComponent<FruitBush>())
                        return State.Gathering;
                }
                else if (attributes.function == Function.Hunter)
                {
                    if (Mathf.Abs(transform.position.x - Target.transform.position.x) < throwDist)
                        AIThrow = true;
                    return State.Hunting;
                }
                if (state == State.Moving)
                {
                    if (attributes.function == Function.Hunter)
                        attributes.function = Function.Gatherer;
                    if (attributes.function == Function.Gatherer && gender != Gender.Female)
                        attributes.function = Function.Hunter;
                }
            }
            else if(currentTribe.leader && currentTribe.leader.name == name && state != State.Migrating)
            {
                migrationDirection = NextDirection((int)currentLevel.x, (int)currentLevel.y);
                return StartMigration();
            }

        }
        return State.Moving;
    }
    #endregion

    #region Wait
    public override void StartWait(float pivot)
    {
        AI.pivot = pivot;
        if (currentTribe.leader && currentTribe.leader.name != name)
            AI.pivot = currentTribe.leader.transform.position.x;
        lastTimeWaited = Time.time;
        Rb.velocity = new Vector2(0, Rb.velocity.y);
        states.waiting = true;
        states.moving = false;
        AIThrow = false;
    }

    public override void AfterWait()
    {
        GetTarget();
        Predator = LookForPredator();
        state = GetState();
        StartAIMoveByState();
        states.waiting = false;
    }
    #endregion

    #region Act
    void GetActions()
    {
        GetActions(Input.GetButtonDown("Fire1"), Input.GetButtonDown("Fire2"), Input.GetButtonDown("Jump"), Input.GetAxisRaw("Horizontal"));

        if (!thrown)
        {
            if (currentMelee)
            {
                Rb.velocity = Vector2.zero;
                StartCoroutine(MeleeAttack(attackCD));
            }
            if (currentRange)
            {
                Rb.velocity = Vector2.zero;
                RangeState(currentRange);
            }
        }
        if (!melee && !range)
            PlayerMove();
        if (!states.eating && feed)
            TakeFood();
        
    }

    void GetActions(bool melee, bool range, bool feed, float move)
    {
        currentMelee = melee;
        currentRange = range;
        AI.currentMove = move;
        this.feed = feed;
    }

    void ResetActions()
    {
        range = false;
        melee = false;
        cooldown = false;
        states.moving = false;
        spearTip.SetActive(false);
    }
    #endregion

    #region Move
    public void PlayerMove()
    {
        states.moving = AI.currentMove != 0;
        MoveRB(AI.currentMove, Rb, attributes.moveSpeed);
    }

    public IEnumerator MoveAtPosition(float x, float threshold)
    {
        control = true;
        states.moving = true;
        while (!OnTarget(x, transform.position.x, threshold))
        {
            AI.currentMove = GetAICurrentMove(x, transform.position.x);
            MoveRB(AI.currentMove, Rb, attributes.moveSpeed);
            AnimationManager();
            yield return new WaitForEndOfFrame();
        }
        states.moving = false;
        control = false;
    }

    public override void ChangingRegion(float target, float position, float speed)
    {
        if (state == State.Flee)
        {
            Predator = LookForPredator();
            if (Predator && LastPredator != Predator)
                Predator = LastPredator;
        }
        if (Predator || state == State.Migrating)
        {
            if (Predator)
                EscapeOppositeToPredator();
            StartAIMoveByState();
            if (OnTarget(target, position, moveThreshold))
            {
                ChangeLevel(NextLvl(migrationDirection));
                states.fled = true;
                lastTimeFled = Time.time;
                if (currentLevel == migrationTargetLvl)
                    StartWait(transform.position.x);
            }
            else
                Moving(target, position, speed);
            if (state == State.Migrating && currentTribe.leader && currentTribe.leader.name != name && currentLevel == currentTribe.leader.currentLevel)
                StartWait(currentTribe.leader.transform.position.x);
        }
        else
            StartWait(transform.position.x);
    }

    public override State StartMigration()
    {
        if (!currentTribe.leader || !currentTribe.leader.gameObject.activeInHierarchy)
            migrationDirection = NextDirection((int)currentLevel.x, (int)currentLevel.y);
        migrationTargetLvl = NextLvl(migrationDirection);
        return State.Migrating;
    }

    public override void StartFlee()
    {
        state = State.Flee;
        LastPredator = Predator;
        states.fled = true;
        lastTimeFled = Time.time;
        EscapeOppositeToPredator();
        StartAIMoveByState();
    }

    #endregion

    #region Eat
    public void TakeFood()
    {
        if (meat)
            meat = null;
        if (bush)
            bush = null;
        TakeClosestFood();
        if (meat)
        {
            Target = meat.gameObject;
            EatMeat();
        }
        else if (bush)
        {
            Target = bush.gameObject;
            EatFruit();
        }
    }
    public override void EatFruit()
    {
        if (Target.GetComponent<FruitBush>() && Target.GetComponent<FruitBush>().fruits.Count > 0 && !states.eating)
        {
            StartCoroutine(FadeOut(true, Target.GetComponent<FruitBush>().fruits[Target.GetComponent<FruitBush>().fruits.Count - 1].transform, .1f));
            Destroy(Target.GetComponent<FruitBush>().fruits[Target.GetComponent<FruitBush>().fruits.Count - 1], 1);
            Target.GetComponent<FruitBush>().fruits.RemoveAt(Target.GetComponent<FruitBush>().fruits.Count - 1);
            if (currentLevel == World.instance.currentLevel)
                StartCoroutine(HUDIconEffect(World.instance.prefabs.HUDIcons[0]));
            StartCoroutine(Eating(eatingTime));
            if (currentFoodNeed > 0)
                currentFoodNeed--;
            else
                currentFoodHeld++;
            lastTimeFed = Time.time;
        }
    }

    public override void EatMeat()
    {
        if (Target.GetComponent<Meat>() && Target.GetComponent<Meat>().meatQuantity > 0 && !states.eating)
        {
            Target.GetComponent<Meat>().meatQuantity--;
            if (currentLevel == World.instance.currentLevel)
                StartCoroutine(HUDIconEffect(World.instance.prefabs.HUDIcons[1]));
            StartCoroutine(Eating(eatingTime));
            if (currentFoodNeed > 0)
                currentFoodNeed--;
            else
                currentFoodHeld++;
            lastTimeFed = Time.time;
        }
    }

    public void TakeClosestFood()
    {
        RaycastHit2D[] hits;
        hits = Physics2D.CircleCastAll(transform.position, 1, Vector2.zero);
        for (int i = 0; i < hits.Length; i++)
        {
            if (hits[i].collider.transform.parent && hits[i].collider.transform.parent.name == "Meat")
            {
                Meat m = hits[i].collider.transform.parent.GetComponent<Meat>();
                if (m && m.meatQuantity > 0 && currentLevel == m.currentLevel)
                    meat = m;
            }
            else if (hits[i].collider.transform.parent && hits[i].collider.transform.parent.name == "Bush")
            {
                FruitBush f = hits[i].collider.transform.parent.GetComponent<FruitBush>();
                if (f && f.fruits.Count > 0 && currentLevel == f.currentLevel)
                    bush = f;
            }
        }
    }
    #endregion

    #region Attack
    public void RangeState(bool input)
    {
        range = input;
        if (range && !thrown)
        {
            attributes.function = Function.Hunter;
            ThrownSpear();
            thrown = true;
            cooldown = true;
            states.moving = false;
            melee = false;
            feed = false;
            lastTimeAttacked = Time.time;
        }
    }

    public void ThrownSpear()
    {
        GameObject obj = Instantiate(World.instance.prefabs.Spear, transform.position, World.instance.prefabs.Spear.transform.rotation);
        obj.transform.localScale = transform.localScale;
        obj.GetComponent<Spear>().currentLevel = currentLevel;
        obj.GetComponent<Spear>().damage = attributes.damage;
        obj.GetComponent<Spear>().sender = gameObject;
        obj.GetComponent<Rigidbody2D>().AddForce((transform.localScale.x > 0 ? attributes.force : new Vector2(-attributes.force.x, attributes.force.y)), ForceMode2D.Force);
        currentSpear = obj.transform;
        Rb.AddForce((transform.localScale.x > 0 ? rangeRecoil : new Vector2(-rangeRecoil.x, rangeRecoil.y)), ForceMode2D.Force);
    }

    public void MeleeState(bool input)
    {
        melee = input;
        if (melee && !thrown)
        {
            states.moving = false;
            Rb.AddForce((transform.localScale.x > 0 ? meleeRecoil : new Vector2(-meleeRecoil.x, meleeRecoil.y)), ForceMode2D.Force);
            cooldown = true;
            lastTimeAttacked = Time.time;
        }
    }
    #endregion

    #region Render
    public override void AnimationManager()
    {

        if (weaponAnimObj && weaponAnimObj.activeInHierarchy)
        {
            if (!thrown)
                weaponAnimObj.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
            else
                weaponAnimObj.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0);
        }

        if (Body.activeInHierarchy)
            base.AnimationManager();
        if (hairAnimObj && hairAnimObj.activeInHierarchy)
            HairAnim.SetBool(walk, states.moving);
        if (facialAnimObj && facialAnimObj.activeInHierarchy)
            FacialAnim.SetBool(walk, states.moving);
        if (weaponAnimObj && weaponAnimObj.activeInHierarchy)
            WeaponAnim.SetBool(walk, states.moving);
        if (Body.activeInHierarchy)
            Anim.SetBool(attack, melee);
        if (hairAnimObj && hairAnimObj.activeInHierarchy)
            HairAnim.SetBool(attack, melee);
        if (facialAnimObj && facialAnimObj.activeInHierarchy)
            FacialAnim.SetBool(attack, melee);
        if (weaponAnimObj && weaponAnimObj.activeInHierarchy)
            WeaponAnim.SetBool(attack, melee);
        if (Body.activeInHierarchy)
            Anim.SetBool(thrw, range);
        if (hairAnimObj && hairAnimObj.activeInHierarchy)
            HairAnim.SetBool(thrw, range);
        if (facialAnimObj && facialAnimObj.activeInHierarchy)
            FacialAnim.SetBool(thrw, range);
        if (weaponAnimObj && weaponAnimObj.activeInHierarchy)
            WeaponAnim.SetBool(thrw, range);

    }

    public override void ToggleView(bool toggle)
    {
        base.ToggleView(toggle);
        Color c = (toggle ? new Color(1, 1, 1, 1) : new Color(1, 1, 1, 0));
        if (hairAnimObj)
            hairAnimObj.GetComponent<SpriteRenderer>().color = c;
        if (facialAnimObj && age >= LifeCycle.Adult)
            facialAnimObj.GetComponentInChildren<SpriteRenderer>().color = c;
        if (weaponAnimObj && gender != Gender.Female && !thrown)
            weaponAnimObj.GetComponentInChildren<SpriteRenderer>().color = c;
        if (Weapon)
            Weapon.SetActive(toggle);
        HUD.gameObject.SetActive(toggle);

    }
    #endregion

    #region Target
    public override GameObject LookForPredator()
    {
        GameObject tmp = null;
        float d = float.MaxValue;
        foreach (Character c in World.instance.levels[(int)currentLevel.y][(int)currentLevel.x].characters)
        {
            float dist = float.MaxValue;
            if (c)
                dist = Mathf.Abs(transform.position.x - c.transform.position.x);
            if (c && dist <= attributes.visionRange
                && c.attributes.function == Function.Hunter
                && c.attributes.type > attributes.type
                && c.currentLevel == currentLevel && c.age < LifeCycle.Dead
                && c.state == State.Hunting
                && c.attributes.type != Type.Human)
            {
                if (dist < d)
                {
                    tmp = c.gameObject;
                    d = dist;
                }
            }
        }

        return tmp;
    }
    #endregion

    #region Hunting
    public override void Hunting(float position, float speed)
    {
        Target = null;
        GetTarget();
        StartAIMoveByState();
        if (Target)
        {
            if (!control && !cooldown)
            {
                if (thrown)
                    StartCoroutine(MoveAtPosition(currentSpear.position.x, moveThreshold));
                else
                    Moving(Target.transform.position.x, position, speed);
                if (!states.meat 
                    && OnTarget(Target.transform.localPosition.x, transform.position.x, throwDist) 
                    && !thrown 
                    && !AIThrow)
                {
                    RangeState(true);
                    AIThrow = true;
                }
                else if (OnTarget(Target.transform.position.x, transform.position.x, (states.meat ? moveThreshold : attributes.attackDist)))
                {
                    if (currentFoodNeed > 0)
                    {
                        if (states.meat)
                        {
                            EatMeat();
                            StartWait(transform.position.x);
                        }
                        else if (!thrown && Target.GetComponent<Character>().age < LifeCycle.Dead)
                            StartCoroutine(MeleeAttack(attackCD));
                    }
                }
            }
        }
        else if(!thrown)
            StartWait(transform.position.x);
        else if(thrown && !cooldown)
            StartCoroutine(MoveAtPosition(currentSpear.position.x, moveThreshold));
    }
        
    IEnumerator MeleeAttack(float time)
    {
        MeleeState(true);
        control = true;
        AnimationManager();
        spearTip.SetActive(true);
        yield return new WaitForSeconds(time);
        control = false;
    }

    public override void AttackPrey()
    {
        if (ValidPrey())
        {
            if (!states.attacking)
            {
                Rb.AddForce((transform.localScale.x > 0 ? attributes.force : new Vector2(-attributes.force.x, attributes.force.y)), ForceMode2D.Force);
                AttackSoundByType();
                StartCoroutine(LookForHit());
                StartCoroutine(Attacking(attackCD));
            }
            else
                StartWait(transform.position.x);
        }
    }
    #endregion
}
